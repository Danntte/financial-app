const colors = require('tailwindcss/colors');
const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    purge: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.vue',

    ],

    theme: {
        
        extend: {
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
                assistant: ['Assistant'],
                secular:['Secular One'],
            },
        colors:{
            
            primary: 'var(--color-primary)',
            secondary: 'var(--color-secondary)',
            green2:  '#19DE19',
            green3: '#18A919',
            blue2: '#064789',
            blue3: '#427AA1'

            
        
        },
        
        },
        minHeight: {
            '0': '0',
            '1/4': '25%',
            '1/2': '50%',
            '3/4': '75%',
            'full': '100%',
            '20': '5rem',
           }
    },

    variants: {
        extend: {
            textColor: ['responsive', 'hover', 'focus', 'group-hover'],
            opacity: ['disabled'],
            borderRadius: ['group-hover','hover', 'focus'],
            borderWidth: ['group-hover','hover',, 'focus'],
            borderColor: ['group-hover','active'],
            borderOpacity: ['group-hover','active'],
            borderStyle: ['group-hover','hover',, 'focus'],
        },
    },

    plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')],
};
