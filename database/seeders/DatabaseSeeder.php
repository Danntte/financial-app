<?php

namespace Database\Seeders;

use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\Forum;
use App\Models\Lesson;
use App\Models\MediaStorage;
use App\Models\Rate;
use App\Models\User;

use App\Models\Order;
use App\Models\Oportunity;
use App\Models\Product;
use App\Models\Provider;
use App\Models\Form;
use App\Models\Message;
use App\Models\MessageRoom;
use App\Models\ProductCategory;
use App\Models\Quotation;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->create([
            'firstname' => 'Pedro',
            'lastname' => 'Montaño',
            'email' => 'admin@test.com',
            'password' => bcrypt('123456'),
            //'admin' => true,
            //'agent' => false,
            'user_type' => 2,
            'birthday' => '1995-04-19 00:00:00',
            'gender' => 'MALE',
            'telephone' => '7508389',
            'mobile_number' => '3214828849',
            ]);

        User::factory()->create([
            'firstname' => 'Alejandro',
            'lastname' => 'Herrera',
            'email' => 'agent@test.com',
            'password' => bcrypt('123456'),
            //'admin' => false,
            //'agent' => true,
            'user_type' => 1,
            'birthday' => '1995-04-19 00:00:00',
            'gender' => 'MALE',
            'telephone' => '7508389',
            'mobile_number' => '3214828849',   
        ]);
        User::factory()->create([
            'firstname' => 'Rorris',
            'lastname' => 'Ortiz',
            'email' => 'user@test.com',
            'password' => bcrypt('123456'),
            'user_type' => 0,
            'birthday' => '1995-04-19 00:00:00',
            'gender' => 'MALE',
            'telephone' => '7508389',
            'mobile_number' => '3214828849',
        ]);

        User::factory(20)->create();
        CourseCategory::factory([
            'name' => "Manejo de emocional",
            'description' => "Cursos para manejo emocional",

        ])->create();
        CourseCategory::factory([
            'name' => "Productos financieros",
            'description' => "Cursos para Productos financieros",

        ])->create();
        CourseCategory::factory([
            'name' => "Guia de la plataforma",
            'description' => "Cursos para guiarse por la plataforma",

        ])->create();

        CourseCategory::factory([
            'name' => "Guia de la atencion al cliente",
            'description' => "Cursos para guiar al cliente de la mejor manera",

        ])->create();

        CourseCategory::factory([
            'name' => "Normativas",
            'description' => "Cursos para saber sobre normativas",

        ])->create();

        MediaStorage::factory(5)->create();
        MediaStorage::factory()->create([
            'name' => "documents/p4wqdgt0NkcRWHDDJASrKAKDZ4EZvbFiiR7I1GF5.pdf",
            'display_name'=> "Titulo X Res26878de8Jun2020 (1).pdf",
            'type'=>'pdf',
            'description'=>'Form',
            'storage'=>'documents'
        ]);
        
        MediaStorage::factory()->create([
            'name' => "images/FmK9lK7dEyneVcxMYtH7gKB5l0TGeMtqGkPp1MSB.png",
            'display_name'=> "mapfre_logo.pdf",
            'type'=>'png',
            'description'=>'mapfre_logo',
            'storage'=>'images'
        ]);
        MediaStorage::factory()->create([
            'name' => "images/iSXSIdChqnBBkKof0H2rWc9L53XyCBmTaAViyQ5u.png",
            'display_name'=> "allianz_logo.pdf",
            'type'=>'png',
            'description'=>'allianz_logo',
            'storage'=>'images'
        ]);
        MediaStorage::factory()->create([
            'name' => "images/9onxErchRW37tiTmohXnPjl1mWhRi5x8QxOQ2agK.png",
            'display_name'=> "zurich_logo.pdf",
            'type'=>'png',
            'description'=>'zurich_logo',
            'storage'=>'images'
        ]);
        MediaStorage::factory()->create([
            'name' => "images/bu95hvSHWYCQ4hpt2yDZPfCfCtM0ObmJsdaf2Rqs.png",
            'display_name'=> "sde_logo.pdf",
            'type'=>'png',
            'description'=>'sde_logo',
            'storage'=>'images'
        ]);

        Course::factory([
            'name' => 'Finanzas personales', //random name
            'slug' => 'finanzas_personales', //ramdom email
            'visible' => 1, 
            'media_storage_id' => MediaStorage::all()->random()->id,
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in lacus ac metus cursus interdum ac quis lectus. Morbi consectetur finibus justo nec facilisis. Quisque pretium elit id condimentum tempus. Nunc commodo placerat efficitur. Pellentesque sapien dui, hendrerit id nibh sed, condimentum porta turpis. Sed vitae leo eu erat tincidunt bibendum. Fusce imperdiet neque non elit ultrices, eget iaculis metus sodales. Ut ac auctor ex. Aenean ultricies augue nisi, vel malesuada nisl tincidunt nec. Nulla hendrerit sed leo id vestibulum. Cras posuere, turpis sed scelerisque dignissim, ipsum neque gravida erat, at accumsan ex libero quis nunc. Nunc nulla velit, consequat eget maximus vestibulum, finibus eu neque. In hac habitasse platea dictumst.', 
            'rate' => 5,// random number,

            'user_id' => User::all()->random()->id,
            'course_category_id' => CourseCategory::all()->random()->id,
        ])->create();

        Course::factory([
            'name' => 'Economía', //random name
            'slug' => 'Economía', //ramdom email
            'visible' => 1, 
            'media_storage_id' => MediaStorage::all()->random()->id,
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in lacus ac metus cursus interdum ac quis lectus. Morbi consectetur finibus justo nec facilisis. Quisque pretium elit id condimentum tempus. Nunc commodo placerat efficitur. Pellentesque sapien dui, hendrerit id nibh sed, condimentum porta turpis. Sed vitae leo eu erat tincidunt bibendum. Fusce imperdiet neque non elit ultrices, eget iaculis metus sodales. Ut ac auctor ex. Aenean ultricies augue nisi, vel malesuada nisl tincidunt nec. Nulla hendrerit sed leo id vestibulum. Cras posuere, turpis sed scelerisque dignissim, ipsum neque gravida erat, at accumsan ex libero quis nunc. Nunc nulla velit, consequat eget maximus vestibulum, finibus eu neque. In hac habitasse platea dictumst.', 
            'rate' => 5,// random number,

            'user_id' => User::all()->random()->id,
            'course_category_id' => CourseCategory::all()->random()->id,
        ])->create();

        Course::factory([
            'name' => 'Manejo de clientes', //random name
            'slug' => 'Manejo_de_clientes', //ramdom email
            'visible' => 1, 
            'media_storage_id' => MediaStorage::all()->random()->id,
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in lacus ac metus cursus interdum ac quis lectus. Morbi consectetur finibus justo nec facilisis. Quisque pretium elit id condimentum tempus. Nunc commodo placerat efficitur. Pellentesque sapien dui, hendrerit id nibh sed, condimentum porta turpis. Sed vitae leo eu erat tincidunt bibendum. Fusce imperdiet neque non elit ultrices, eget iaculis metus sodales. Ut ac auctor ex. Aenean ultricies augue nisi, vel malesuada nisl tincidunt nec. Nulla hendrerit sed leo id vestibulum. Cras posuere, turpis sed scelerisque dignissim, ipsum neque gravida erat, at accumsan ex libero quis nunc. Nunc nulla velit, consequat eget maximus vestibulum, finibus eu neque. In hac habitasse platea dictumst.', 
            'rate' => 5,// random number,

            'user_id' => User::all()->random()->id,
            'course_category_id' => CourseCategory::all()->random()->id,
        ])->create();

        Course::factory([
            'name' => 'Normativa colombiana', //random name
            'slug' => 'Normativa_colombiana', //ramdom email
            'visible' => 1, 
            'media_storage_id' => MediaStorage::all()->random()->id,
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in lacus ac metus cursus interdum ac quis lectus. Morbi consectetur finibus justo nec facilisis. Quisque pretium elit id condimentum tempus. Nunc commodo placerat efficitur. Pellentesque sapien dui, hendrerit id nibh sed, condimentum porta turpis. Sed vitae leo eu erat tincidunt bibendum. Fusce imperdiet neque non elit ultrices, eget iaculis metus sodales. Ut ac auctor ex. Aenean ultricies augue nisi, vel malesuada nisl tincidunt nec. Nulla hendrerit sed leo id vestibulum. Cras posuere, turpis sed scelerisque dignissim, ipsum neque gravida erat, at accumsan ex libero quis nunc. Nunc nulla velit, consequat eget maximus vestibulum, finibus eu neque. In hac habitasse platea dictumst.', 
            'rate' => 5,// random number,

            'user_id' => User::all()->random()->id,
            'course_category_id' => CourseCategory::all()->random()->id,
        ])->create();

        Course::factory([
            'name' => 'Normativa internacional', //random name
            'slug' => 'Normativa_internacional', //ramdom email
            'visible' => 1, 
            'media_storage_id' => MediaStorage::all()->random()->id,
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in lacus ac metus cursus interdum ac quis lectus. Morbi consectetur finibus justo nec facilisis. Quisque pretium elit id condimentum tempus. Nunc commodo placerat efficitur. Pellentesque sapien dui, hendrerit id nibh sed, condimentum porta turpis. Sed vitae leo eu erat tincidunt bibendum. Fusce imperdiet neque non elit ultrices, eget iaculis metus sodales. Ut ac auctor ex. Aenean ultricies augue nisi, vel malesuada nisl tincidunt nec. Nulla hendrerit sed leo id vestibulum. Cras posuere, turpis sed scelerisque dignissim, ipsum neque gravida erat, at accumsan ex libero quis nunc. Nunc nulla velit, consequat eget maximus vestibulum, finibus eu neque. In hac habitasse platea dictumst.', 
            'rate' => 5,// random number,

            'user_id' => User::all()->random()->id,
            'course_category_id' => CourseCategory::all()->random()->id,
        ])->create();

        Course::factory([
            'name' => 'Productos Financieros', //random name
            'slug' => 'Productos_Financieros', //ramdom email
            'visible' => 1, 
            'media_storage_id' => MediaStorage::all()->random()->id,
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in lacus ac metus cursus interdum ac quis lectus. Morbi consectetur finibus justo nec facilisis. Quisque pretium elit id condimentum tempus. Nunc commodo placerat efficitur. Pellentesque sapien dui, hendrerit id nibh sed, condimentum porta turpis. Sed vitae leo eu erat tincidunt bibendum. Fusce imperdiet neque non elit ultrices, eget iaculis metus sodales. Ut ac auctor ex. Aenean ultricies augue nisi, vel malesuada nisl tincidunt nec. Nulla hendrerit sed leo id vestibulum. Cras posuere, turpis sed scelerisque dignissim, ipsum neque gravida erat, at accumsan ex libero quis nunc. Nunc nulla velit, consequat eget maximus vestibulum, finibus eu neque. In hac habitasse platea dictumst.', 
            'rate' => 5,// random number,

            'user_id' => User::all()->random()->id,
            'course_category_id' => CourseCategory::all()->random()->id,
        ])->create();

        Course::factory([
            'name' => 'Seguros de vida', //random name
            'slug' => 'Seguros_de_vida', //ramdom email
            'visible' => 1, 
            'media_storage_id' => MediaStorage::all()->random()->id,
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in lacus ac metus cursus interdum ac quis lectus. Morbi consectetur finibus justo nec facilisis. Quisque pretium elit id condimentum tempus. Nunc commodo placerat efficitur. Pellentesque sapien dui, hendrerit id nibh sed, condimentum porta turpis. Sed vitae leo eu erat tincidunt bibendum. Fusce imperdiet neque non elit ultrices, eget iaculis metus sodales. Ut ac auctor ex. Aenean ultricies augue nisi, vel malesuada nisl tincidunt nec. Nulla hendrerit sed leo id vestibulum. Cras posuere, turpis sed scelerisque dignissim, ipsum neque gravida erat, at accumsan ex libero quis nunc. Nunc nulla velit, consequat eget maximus vestibulum, finibus eu neque. In hac habitasse platea dictumst.', 
            'rate' => 5,// random number,

            'user_id' => User::all()->random()->id,
            'course_category_id' => CourseCategory::all()->random()->id,
        ])->create();

        Course::factory([
            'name' => 'Seguros de vehiculos', //random name
            'slug' => 'Seguros_de_vehiculos', //ramdom email
            'visible' => 1, 
            'media_storage_id' => MediaStorage::all()->random()->id,
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in lacus ac metus cursus interdum ac quis lectus. Morbi consectetur finibus justo nec facilisis. Quisque pretium elit id condimentum tempus. Nunc commodo placerat efficitur. Pellentesque sapien dui, hendrerit id nibh sed, condimentum porta turpis. Sed vitae leo eu erat tincidunt bibendum. Fusce imperdiet neque non elit ultrices, eget iaculis metus sodales. Ut ac auctor ex. Aenean ultricies augue nisi, vel malesuada nisl tincidunt nec. Nulla hendrerit sed leo id vestibulum. Cras posuere, turpis sed scelerisque dignissim, ipsum neque gravida erat, at accumsan ex libero quis nunc. Nunc nulla velit, consequat eget maximus vestibulum, finibus eu neque. In hac habitasse platea dictumst.', 
            'rate' => 5,// random number,

            'user_id' => User::all()->random()->id,
            'course_category_id' => CourseCategory::all()->random()->id,
        ])->create();

        Course::factory([
            'name' => 'Curso basico', //random name
            'slug' => 'Curso_basico', //ramdom email
            'visible' => 1, 
            'media_storage_id' => MediaStorage::all()->random()->id,
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in lacus ac metus cursus interdum ac quis lectus. Morbi consectetur finibus justo nec facilisis. Quisque pretium elit id condimentum tempus. Nunc commodo placerat efficitur. Pellentesque sapien dui, hendrerit id nibh sed, condimentum porta turpis. Sed vitae leo eu erat tincidunt bibendum. Fusce imperdiet neque non elit ultrices, eget iaculis metus sodales. Ut ac auctor ex. Aenean ultricies augue nisi, vel malesuada nisl tincidunt nec. Nulla hendrerit sed leo id vestibulum. Cras posuere, turpis sed scelerisque dignissim, ipsum neque gravida erat, at accumsan ex libero quis nunc. Nunc nulla velit, consequat eget maximus vestibulum, finibus eu neque. In hac habitasse platea dictumst.', 
            'rate' => 5,// random number,

            'user_id' => User::all()->random()->id,
            'course_category_id' => CourseCategory::all()->random()->id,
        ])->create();

        Course::factory([
            'name' => 'Curso avanzado', //random name
            'slug' => 'Curso_avanzado', //ramdom email
            'visible' => 1, 
            'media_storage_id' => MediaStorage::all()->random()->id,
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in lacus ac metus cursus interdum ac quis lectus. Morbi consectetur finibus justo nec facilisis. Quisque pretium elit id condimentum tempus. Nunc commodo placerat efficitur. Pellentesque sapien dui, hendrerit id nibh sed, condimentum porta turpis. Sed vitae leo eu erat tincidunt bibendum. Fusce imperdiet neque non elit ultrices, eget iaculis metus sodales. Ut ac auctor ex. Aenean ultricies augue nisi, vel malesuada nisl tincidunt nec. Nulla hendrerit sed leo id vestibulum. Cras posuere, turpis sed scelerisque dignissim, ipsum neque gravida erat, at accumsan ex libero quis nunc. Nunc nulla velit, consequat eget maximus vestibulum, finibus eu neque. In hac habitasse platea dictumst.', 
            'rate' => 5,// random number,

            'user_id' => User::all()->random()->id,
            'course_category_id' => CourseCategory::all()->random()->id,
        ])->create();

        Course::factory([
            'name' => 'Curso para seguros personales', //random name
            'slug' => 'Curso_para_seguros_personales', //ramdom email
            'visible' => 1, 
            'media_storage_id' => MediaStorage::all()->random()->id,
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in lacus ac metus cursus interdum ac quis lectus. Morbi consectetur finibus justo nec facilisis. Quisque pretium elit id condimentum tempus. Nunc commodo placerat efficitur. Pellentesque sapien dui, hendrerit id nibh sed, condimentum porta turpis. Sed vitae leo eu erat tincidunt bibendum. Fusce imperdiet neque non elit ultrices, eget iaculis metus sodales. Ut ac auctor ex. Aenean ultricies augue nisi, vel malesuada nisl tincidunt nec. Nulla hendrerit sed leo id vestibulum. Cras posuere, turpis sed scelerisque dignissim, ipsum neque gravida erat, at accumsan ex libero quis nunc. Nunc nulla velit, consequat eget maximus vestibulum, finibus eu neque. In hac habitasse platea dictumst.', 
            'rate' => 5,// random number,

            'user_id' => User::all()->random()->id,
            'course_category_id' => CourseCategory::all()->random()->id,
        ])->create();

        
        Lesson::factory(100)->create();
        Rate::factory(100)->create();
        Forum::factory(200)->create();

        Provider::factory([
            'name' => 'Mapfre', 
            'email' => 'mapfre@mapfre.com',
            'phone' => '321456987', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'media_storage_id' => MediaStorage::all()->random()->id,
        ])->create();
        Provider::factory([
            'name' => 'Zurich Insurance Group', 
            'email' => 'Zurich@Zurich.com',
            'phone' => '321456987', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'media_storage_id' => MediaStorage::all()->random()->id,
        ])->create();
        Provider::factory([
            'name' => 'Allianz', 
            'email' => 'Allianz@Allianz.com',
            'phone' => '321456987', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'media_storage_id' => MediaStorage::all()->random()->id,
        ])->create();
        Provider::factory([
            'name' => 'Seguros del estado', 
            'email' => 'sde@sde.com',
            'phone' => '321456987', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'media_storage_id' => MediaStorage::all()->random()->id,
        ])->create();
       
        ProductCategory::factory()->create([
            'name' => 'Seguros de vida' ,
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'business_type'=> 1,
            
        ]);
        ProductCategory::factory()->create([
            'name' => 'Seguros de vehiculos' ,
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'business_type'=> 1,
        ]);
        ProductCategory::factory()->create([
            'name' => 'Seguros personales' ,
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'business_type'=> 1,
        ]);
        ProductCategory::factory()->create([
            'name' => 'Seguros de hogar' ,
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'business_type'=> 1,
        ]);
        ProductCategory::factory()->create([
            'name' => 'Hipotecarios' ,
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'business_type'=> 2,
        ]);
        ProductCategory::factory()->create([
            'name' => 'Consumo' ,
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'business_type'=> 2,
        ]);
        ProductCategory::factory()->create([
            'name' => 'vehículos' ,
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'business_type'=> 2,
        ]);

        //ProductCategory::factory(4)->create();
        
        Product::factory()->create([
            'name' => 'seguro de vida', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'product_category_id' => '1',
            
            
        ]);
        Product::factory()->create([
            'name' => 'Seguro de vida a termino', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'product_category_id' => '1',
            
            
        ]);
        Product::factory()->create([
            'name' => 'Seguro de accidentes personales', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'product_category_id' => '1',
            
            
        ]);
        Product::factory()->create([
            'name' => 'Seguro de vida deudor', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'product_category_id' => '1',
            
            
        ]);
        Product::factory()->create([
            'name' => 'Soat', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'product_category_id' => '2',
            
            
        ]);
        Product::factory()->create([
            'name' => 'Seguro todo riesgo para vehículo',
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'product_category_id' => '2',
            
            
        ]);
        Product::factory()->create([
            'name' => 'Seguro RCE vehículos', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'product_category_id' => '2',
            
            
        ]);
        Product::factory()->create([
            'name' => 'Seguros exequiales', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'product_category_id' => '3',
            
            
        ]);
        Product::factory()->create([
            'name' => 'Seguros de salud', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'product_category_id' => '3',
            
            
        ]);
        Product::factory()->create([
            'name' => 'Seguros de viajes internacionales', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'product_category_id' => '3',
            
            
        ]);
        Product::factory()->create([
            'name' => 'RCE para médicos y profesionales', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'product_category_id' => '3',
            
            
        ]);
        Product::factory()->create([
            'name' => 'Hogar tradicional', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'product_category_id' => '4',
            
            
        ]);
        Product::factory()->create([
            'name' => 'Hogar deudor', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'product_category_id' => '4',
            
            
        ]);
        Product::factory()->create([
            'name' => 'Arrendamiento', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'product_category_id' => '4',
            
            
        ]);
        Product::factory()->create([
            'name' => 'Hogar tradicional', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'product_category_id' => '4',
            
            
        ]);
        Product::factory()->create([
            'name' => 'Vivienda nueva ', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'product_category_id' => '5',
            
            
        ]);
        Product::factory()->create([
            'name' => 'Vivienda usada', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'product_category_id' => '5',
            
            
        ]);
        Product::factory()->create([
            'name' => 'Libre Inversión', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'product_category_id' => '6',
            
            
        ]);

        Product::factory()->create([
            'name' => 'Vehículo nuevo', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'product_category_id' => '7',
            
            
        ]);
        Product::factory()->create([
            'name' => 'Vehículo usado', 
            'description'=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro error ipsa perspiciatis dolor, odit mollitia, quo minima doloribus ratione autem qui accusantium, facilis eligendi? Qui voluptas dicta iste est!',
            'status' => true, 
            'product_category_id' => '7',
            
            
        ]);
       
        //Product::factory(50)->create();
        Form::factory(100)->create()->each(
            function($form){//funcion anonima para "attachar" por la tabla pivote
                $mediaStorage = MediaStorage::all()->random(mt_rand(1,5))->pluck('id');//se buscan las id de entre 1 y 8 medias
                $form->mediaStorage()->attach($mediaStorage);//se "attachan" esos proveedores a un producto
            }
        );
        
        Oportunity::factory()->create([
            'name' => 'Pedro',
            'email' => 'user@test.com',
            'product_id' => Product::all()->random()->id,

        ]);
        Oportunity::factory()->create([
            'name' => 'Pedro',
            'email' => 'admin@test.com',
            'product_id' => Product::all()->random()->id,
            
        ]);
        Oportunity::factory(20)->create();
        Order::factory()->create([
            'product_id' => Product::all()->random()->id,
            'category_product_id' => ProductCategory::all()->random()->id,
            'client_id' => User::where('user_type','=',0)->get()->random()->id,
            'seller_id' => User::all()->where('user_type','>',0)->random()->id,
            'traking_id' => 12121212,
            'status' => 'En espera de cotización',
            'buttonStatus' => 0,
            'amount' => 5555555,
            
            //'media_storage_id' => MediaStorage::all()->random()->id,
            'quotation_title' => 'Seguro todo riesgo',
            'quotation_date' => '2010-12-01 00:00:00',
            'client_name' => 'Pedro Alejandro',
            'customer_phone' => '123456789',
            'id_type' => 'CC',
            'id_number' => '1016112769',
            'date_of_birth' => '2010-12-01 00:00:00',
            'gender' => 'Femenino',
            
            'title_insurance_card_1' => 'Tipo de carro:',
            'title_insurance_card_2' => 'Modelo:',
            'title_insurance_card_3' => 'Placa:',
            'title_insurance_card_4' => 'Código Fasecolda:',
            'title_insurance_card_5' => 'Zona de tarifacción:',
            'description_insurance_card_1' => '888',
            'description_insurance_card_2' => '2018',
            'description_insurance_card_3' => 'DFE896',
            'description_insurance_card_4' => '045688621',
            'description_insurance_card_5' => 'Bogotá, Cundinamarca',

            'title_value_insurance_card_1' => 'Valor asegurado:',
            'insured_value' => '34300000',
            

            'email' => 'user@test.com',
            'address' => 'calle falsa 123',

            'title_1' => 'primer titulo',
            'title_2' => 'segundo titulo',
            'title_3' => 'tercer titulo',
            'title_4' => 'cuarto titulo',
            'title_5' => 'quinto titulo',
            
            'link_payment' => 'https://soat.segurosfalabella.com.co/sale/step0?origen=AutoPautaDigital&gclid=Cj0KCQjw7MGJBhD-ARIsAMZ0eeum7uzWsN3MHBzEclxsGBPrjWE2mdygpsAh_7cX-s_UgiDN37FN6aUaAp0eEALw_wcB',
            
            'oportunity_id' => 1
            
        ])->each(
            function($order){//funcion anonima para "attachar" por la tabla pivote
                $mediaStorage = MediaStorage::all()->random(mt_rand(1,5))->pluck('id');//se buscan las id de entre 1 y 8 medias
                $order->mediaStorage()->attach($mediaStorage);//se "attachan" esos proveedores a un producto
            }
        );
        Order::factory(20)->create()->each(
            function($order){//funcion anonima para "attachar" por la tabla pivote
                $mediaStorage = MediaStorage::all()->random(mt_rand(1,5))->pluck('id');//se buscan las id de entre 1 y 8 medias
                $order->mediaStorage()->attach($mediaStorage);//se "attachan" esos proveedores a un producto
            }
        );
        Quotation::factory()->create([
            'order_id' => 1,
            'form_id' => Form::all()->random()->id,
            'sale_price' => 5555555,
            'description_1' => 'primera descripcion ',
            'description_2' => 'segunda descripcion ',
            'description_3' => 'tercera descripcion ',
            'description_4' => 'cuarta descripcion ',
            'description_5' => 'quinta descripcion ',
            'status' => false,
            'media_storage_id' => MediaStorage::all()->random()->id,
        ]);
        Quotation::factory(50)->create();
        
        //Seeder of messages, only admin on order 1
        MessageRoom::factory(2)->create();
        Message::factory(10)->create();

    }
}
