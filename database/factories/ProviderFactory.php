<?php

namespace Database\Factories;

use App\Models\MediaStorage;
use App\Models\Provider;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProviderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Provider::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name' => $this->faker->name, //random name
            'email' => $this->faker->unique()->safeEmail(),
            'phone' => $this->faker->phoneNumber, // random number
            'description'=> $this->faker->text(800),
            'status' => $this->faker->boolean, 
            'media_storage_id' => MediaStorage::all()->random()->id,

        ];
    }
}
