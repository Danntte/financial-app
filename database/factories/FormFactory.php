<?php

namespace Database\Factories;

use App\Models\Form;
use App\Models\MediaStorage;
use App\Models\Product;
use App\Models\Provider;
use Illuminate\Database\Eloquent\Factories\Factory;

class FormFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Form::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            
            'description'=> $this->faker->text(800),
            'provider_id' => Provider::all()->random()->id,
            'product_id' => Product::all()->random()->id,
            'description_1' => $this->faker->text(10),
            'description_2' => $this->faker->text(10),
            'description_3' => $this->faker->text(10),
            'description_4' => $this->faker->text(20),
            'description_5' => $this->faker->text(30),
            
        ];
    }
}
