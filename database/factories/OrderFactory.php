<?php

namespace Database\Factories;

use App\Models\MediaStorage;
use App\Models\Oportunity;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;


class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'product_id' => Product::all()->random()->id,
            'category_product_id' => ProductCategory::all()->random()->id,
            'client_id' =>$this->faker->numberBetween(10,20),
            'seller_id' => User::all()->where('user_type','>',0)->random()->id,
            'traking_id' => $this->faker->numberbetween(11111111,99999999),
            'status' => $this->faker->randomElement(['En espera de cotización','Cotización entregada', 'Orden en gestión', 'Orden en espera', 'Aprobada o emitida', 'Orden cancelada', 'Orden finalizada']),
            'buttonStatus' => $this->faker->numberBetween(0,9),
            'amount' => $this->faker->numberbetween(1111111,5555555),
            'buy_date' => $this->faker->date(),
            'due_date'  => $this->faker->date(),
            //'media_storage_id' => MediaStorage::all()->random()->id,

            'quotation_date' => $this->faker->date(),
            'quotation_title' => $this->faker->text(20),
            'client_name' => $this->faker->name(),
            'customer_phone' => $this->faker->phoneNumber,
            'id_type' => $this->faker->randomElement(['CC','TI', 'RC', 'CE', 'PA']),
            'id_number' => $this->faker->numberbetween(11111111,99999999),
            'date_of_birth' => $this->faker->date(),
            'gender' => $this->faker->randomElement(['Masculino','Femenino', 'Sin especificar']),
            
            'title_insurance_card_1'=> $this->faker->text(10) ,
            'title_insurance_card_2'=> $this->faker->text(10) ,
            'title_insurance_card_3'=> $this->faker->text(10) ,
            'title_insurance_card_4'=> $this->faker->text(10) ,
            'title_insurance_card_5'=> $this->faker->text(10) ,
            'description_insurance_card_1'=> $this->faker->text(10) ,
            'description_insurance_card_2'=> $this->faker->text(10) ,
            'description_insurance_card_3'=> $this->faker->text(10) ,
            'description_insurance_card_4'=> $this->faker->text(10) ,
            'description_insurance_card_5'=> $this->faker->text(10) ,

            'title_value_insurance_card_1'  => $this->faker->text(10),
            'insured_value' => $this->faker->numberbetween(1111111,5555555),
            



            'email' => $this->faker->unique()->safeEmail(),
            'address' => $this->faker->address(),

            'title_1' => $this->faker->text(20),
            'title_2' => $this->faker->text(20),
            'title_3' => $this->faker->text(20),
            'title_4' => $this->faker->text(20),
            'title_5' => $this->faker->text(20),

            'link_payment' => $this->faker->text(20),
            
            'oportunity_id' => Oportunity::where('id',">",2)->get()->random()->id
            
           
            
            
            
            
           
            

            
            
           
       
           
            
            
            
           
            
        ];
    }
}
