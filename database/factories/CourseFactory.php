<?php

namespace Database\Factories;

use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\MediaStorage;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CourseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Course::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name, //random name
            'slug' => $this->faker->slug, //ramdom email
            'visible' => $this->faker->boolean, 
            'media_storage_id' => MediaStorage::all()->random()->id,
            'description' => $this->faker->text(800), 
            'rate' => $this->faker->numberBetween(1 , 5),// random number,

            'user_id' => User::all()->random()->id,
            'course_category_id' => CourseCategory::all()->random()->id,
            
        ];
    }
}
