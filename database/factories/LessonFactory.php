<?php

namespace Database\Factories;

use App\Models\Course;
use App\Models\Lesson;

use Illuminate\Database\Eloquent\Factories\Factory;

class LessonFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Lesson::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name, //random name
            //'forum' => $this->faker->text(800), //ramdom forum
            
            'description' => $this->faker->text(800),
            'visible' => $this->faker->boolean, 
            'video'=> $this->faker->randomElement(["MtT5_PgLJlY","vlDzYIIOYmM","gh051HppxSk","uXUVGhqwywE","x76VEPXYaI0"]),

            
            'course_id' => Course::all()->random()->id,
        ];
    }
}
