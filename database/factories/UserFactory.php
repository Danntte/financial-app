<?php

namespace Database\Factories;

use App\Models\Admin;
use App\Models\Agent;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'firstname' => $this->faker->name, //random name
            'lastname' => $this->faker->lastName, //random lastname
            'email' => $this->faker->unique()->safeEmail(),
            'birthday' => $this->faker->date(), // ramnom date
            'gender' => $this->faker->randomElement([User::GENDER_FEMALE, User::GENDER_MALE, User::GENDER_UNSPECIFIED]), //empty should have a coherent random value
            'telephone' => $this->faker->phoneNumber, // random number
            'mobile_number' => $this->faker->phoneNumber, // random number
            //'admin' => $admin = $this->faker->boolean, // ramdom admin
            //'agent' => $admin = $this->faker->boolean, // ramdom admin
            'user_type' => $this->faker->randomElement([Admin::USER_NO_ADMIN, Admin::USER_IS_ADMIN, Agent::USER_IS_AGENT]),

            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
