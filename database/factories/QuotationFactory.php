<?php

namespace Database\Factories;

use App\Models\Form;
use App\Models\MediaStorage;
use App\Models\Order;
use App\Models\Quotation;
use Illuminate\Database\Eloquent\Factories\Factory;

class QuotationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Quotation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'order_id' => Order::all()->random()->id,
            'form_id' => Form::all()->random()->id,
            'sale_price' => $this->faker->numberbetween(1111111,5555555),
            'description_1' => $this->faker->text(10),
            'description_2' => $this->faker->text(10),
            'description_3' => $this->faker->text(10),
            'description_4' => $this->faker->text(20),
            'description_5' => $this->faker->text(30),
            'status' => $this->faker->boolean(),
            'media_storage_id' => MediaStorage::all()->random()->id,

            
        ];
    }
}
