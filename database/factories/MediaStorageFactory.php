<?php

namespace Database\Factories;

use App\Models\MediaStorage;
use Illuminate\Database\Eloquent\Factories\Factory;

class MediaStorageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MediaStorage::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => "images/nuDQVna0fLQATx076dPPA0Ticlhm2RPOjRwL50tl.png",
            'display_name'=> "HSV_color_solid_cylinder.png",
            'type'=>'png',
            'description'=>$this->faker->randomElement(['Documents','Validation','Form','Other']),
            'storage'=>'images'
            
        ];
    }
}
