<?php

namespace Database\Factories;

use App\Models\Forum;
use App\Models\Lesson;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ForumFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Forum::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            
            
            'content' => $this->faker->text(800),
            'user_id' => User::all()->random()->id,
            'lesson_id' => Lesson::all()->random()->id,
        ];
    }
}
