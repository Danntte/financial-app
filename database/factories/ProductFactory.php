<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Form;
use App\Models\Provider;
use App\Models\ProductCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [

            'name' => $this->faker->name, //random name
            'description'=> $this->faker->text(800),
            'status' => $this->faker->boolean, 
            'product_category_id' => ProductCategory::all()->random()->id,
            
            

            //
        ];
    }
}
