<?php

namespace Database\Factories;

use App\Models\Oportunity;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class OportunityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Oportunity::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name' => $this->faker->name(),
            'customer_phone' => $this->faker->phoneNumber,
            'address'=> $this->faker->address,
            'date_of_birth' => $this->faker->date(),
            'subject' => $this->faker->numberBetween(1,4),
            'email' => $this->faker->unique()->safeEmail(),
            'message' => $this->faker->text(),
            'product_id' => Product::all()->random()->id,
            'client_id' =>$this->faker->numberBetween(10,20),
            'seller_id' => User::all()->where('user_type','>',0)->random()->id,
            'status' => $this->faker->randomElement(['Abierta','Cerrada', 'En revisión']),
            'opening' => $this->faker->date(),
            'clousure'  => $this->faker->date(),
        ];
    }
}
