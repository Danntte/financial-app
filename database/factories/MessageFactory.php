<?php

namespace Database\Factories;

use App\Models\Message;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

class MessageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Message::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'message_rooms_id' => $this->faker->randomElement(['1','2']),
            'user_id' => User::where('email', '=', 'admin@test.com')->get()[0]->id,
            'message' => $this->faker->words(3, true),
        ];
    }
}
