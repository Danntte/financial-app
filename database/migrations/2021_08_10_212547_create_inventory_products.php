<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoryProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('abstract');
            $table->string('description');
            $table->integer('price');
            $table->unsignedBigInteger('owner');
            $table->unsignedBigInteger('sale');
            $table->boolean('status');
            $table->timestamps();
            $table->string('category', 20);
            $table->unsignedBigInteger('media_storage_id')->unsigned()->nullable();

            $table->foreign('owner')->references('id')->on('users');
            $table->foreign('media_storage_id')->references('id')->on('media_storage');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_products');
    }
}
