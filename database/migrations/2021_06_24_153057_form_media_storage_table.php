<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FormMediaStorageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_media_storage', function (Blueprint $table) {
            $table->unsignedBigInteger('media_storage_id')->unsigned();
            $table->unsignedBigInteger('form_id')->unsigned();

            $table->foreign('media_storage_id')->references('id')->on('media_storage');
            $table->foreign('form_id')->references('id')->on('forms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_media_storage');
    }
}
