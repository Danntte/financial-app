<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->text('description');
            $table->double('rate')->nullable();
            $table->boolean('visible')->default(false);
            
            $table->unsignedBigInteger('media_storage_id')->unsigned()->nullable();
            $table->unsignedBigInteger('user_id')->unsigned();
            $table->unsignedBigInteger('course_category_id')->unsigned();
            

            $table->timestamps();

            /**
             * referencia a claves foraneas
             */
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('course_category_id')->references('id')->on('course_categories');
            $table->foreign('media_storage_id')->references('id')->on('media_storage');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
