<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MediaStorageOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_storage_order', function (Blueprint $table) {
            $table->unsignedBigInteger('media_storage_id')->unsigned();
            $table->unsignedBigInteger('order_id')->unsigned();

            $table->foreign('media_storage_id')->references('id')->on('media_storage');
            $table->foreign('order_id')->references('id')->on('orders');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_storage_order');
    }
}
