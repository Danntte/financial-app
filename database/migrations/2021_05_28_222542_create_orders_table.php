<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();

            $table->string('status')->default('En espera de cotización');
            $table->integer('buttonStatus')->default(0);
            
            $table->timestamp('buy_date')->nullable();
            $table->timestamp('due_date')->nullable();
            $table->string('traking_id')->unique()->nullable();
            $table->bigInteger('amount')->nullable();


            //datos para formulario de cotizacion 
            
            
            $table->string('title_1')->nullable()->default('Descripción 1');
            $table->string('title_2')->nullable()->default('Descripción 2');
            $table->string('title_3')->nullable()->default('Descripción 3');;
            $table->string('title_4')->nullable()->default('Descripción 4');;
            $table->string('title_5')->nullable()->default('Descripción 5');;
            
            $table->string('quotation_title')->nullable();
            $table->timestamp('quotation_date')->nullable();
            $table->string('client_name')->nullable();
            $table->string('customer_phone')->nullable();
            $table->string('id_type')->nullable();
            $table->string('id_number')->nullable();
            $table->timestamp('date_of_birth')->nullable();
            $table->string('gender')->nullable();

            //para formulario de productos
            $table->string('title_insurance_card_1')->nullable();
            $table->string('title_insurance_card_2')->nullable();
            $table->string('title_insurance_card_3')->nullable();
            $table->string('title_insurance_card_4')->nullable();
            $table->string('title_insurance_card_5')->nullable();
            $table->string('description_insurance_card_1')->nullable();
            $table->string('description_insurance_card_2')->nullable();
            $table->string('description_insurance_card_3')->nullable();
            $table->string('description_insurance_card_4')->nullable();
            $table->string('description_insurance_card_5')->nullable();

            $table->string('title_value_insurance_card_1')->nullable();
            $table->bigInteger('insured_value')->nullable();
            
            
            //para formulario de ordenes
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            
            $table->unsignedBigInteger('category_product_id')->unsigned()->nullable();
            $table->unsignedBigInteger('product_id')->unsigned()->nullable();
            $table->unsignedBigInteger('seller_id')->unsigned()->nullable();
            $table->unsignedBigInteger('client_id')->unsigned()->nullable();
            $table->unsignedBigInteger('oportunity_id')->nullable();

            $table->string('link_payment')->nullable();
            
            $table->timestamps();

            //$table->foreign('oportunity_id')->references('id')->on('oportunities');
            $table->foreign('oportunity_id')->references('id')->on('oportunities');
            $table->foreign('seller_id')->references('id')->on('users');
            $table->foreign('client_id')->references('id')->on('users');
            $table->foreign('category_product_id')->references('id')->on('product_categories');
            $table->foreign('product_id')->references('id')->on('products');
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
