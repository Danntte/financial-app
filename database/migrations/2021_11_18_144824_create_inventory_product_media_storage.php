<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoryProductMediaStorage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_product_media_storage', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('media_storage_id');
            $table->unsignedBigInteger('inventory_product_id');

            $table->foreign('media_storage_id')->references('id')->on('media_storage');
            $table->foreign('inventory_product_id')->references('id')->on('inventory_products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_product_media_storage');
    }
}
