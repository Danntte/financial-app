<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LessonMediaStorageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesson_media_storage', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('lesson_id')->unsigned();
            $table->unsignedBigInteger('media_storage_id')->unsigned();

            $table->foreign('lesson_id')->references('id')->on('lessons');
            $table->foreign('media_storage_id')->references('id')->on('media_storage');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_media_storage');
    }
}
