<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOportunityProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oportunity_product', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('oportunity_id')->unsigned();
            $table->unsignedBigInteger('product_id')->unsigned();

            $table->foreign('oportunity_id')->references('id')->on('oportunities');
            $table->foreign('product_id')->references('id')->on('products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oportunity_product');
    }
}
