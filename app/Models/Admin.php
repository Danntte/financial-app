<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admin extends User
{
    use HasFactory;

    CONST USER_IS_ADMIN = 2;
    CONST USER_NO_ADMIN = 0;

    protected $fillable = [
        'firstname',
        'lastname',
        
        'email',
        'password',
        //'verified',
        //'verification_token',
        //'admin',
        //'agent',
        'user_type',

        'birthday',
        'gender',
        'telephone',
        'mobile_number',
    ];

    public function isAdmin()
    {

        return $this->user_type == Admin::USER_IS_ADMIN;
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
        
    }
}
