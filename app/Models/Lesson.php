<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    use HasFactory;

    const LESSON_IS_VISIBLE = TRUE;
    const LESSON_NOT_VISIBLE = FALSE;

    protected $fillable =[
        'name',
        'visible',
        'description',             
        //'forum',
        'course_id',
        'video',
        'media_storage_id'

    ];
    public function mediaStorage()
    {
        return $this->belongsToMany(MediaStorage::class);
    }
    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function forums()
    {
        return $this->hasMany(Forum::class);
    }

    public function isVisible()
    {
        return $this->visible == Lesson::LESSON_IS_VISIBLE;
    }

}
