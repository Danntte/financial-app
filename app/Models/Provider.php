<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    use HasFactory;

    protected $fillable =[
        'name',
        'email',
        'phone',
        'description',
        'status',
        'media_storage_id'

    ];
    public function forms()
    {
        return $this->hasMany(Form::class);
    }
    public function quotations()
    {
        return $this->hasMany(Quotation::class);
    }

    public function mediaStorage()
    {
        return $this->belongsTo(MediaStorage::class);
    }

    

}
