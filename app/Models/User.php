<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    CONST GENDER_MALE = 'MALE';
    CONST GENDER_FEMALE = 'FEMALE';
    CONST GENDER_UNSPECIFIED = 'UNSPECIFIED';

    protected $table = 'users';// users table defined

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        
        'email',
        'password',
        //'verified',
        //'verification_token',
        //'admin',
        //'agent',
        'user_type',

        'birthday',
        'gender',
        'telephone',
        'mobile_number',
        
    ];
    

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        //'verification_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
        'avatar',
        'name',
        
    ];

    public function courses()
    {
        return $this->hasMany(Course::class);
    }
    public function orders()
    {
        return $this->hasMany(Order::class);
    }
    
    public function oportunities()
    {
        return $this->hasMany(Oportunity::class);
    }
    public function getAvatarAttribute() 
    {
        $email = md5($this->email);
        return "https://s.gravatar.com/avatar/$email";
    }
    public function getNameAttribute() 
    {
        return "{$this->firstname} {$this->lastname}";
    }
/*
    public function isVerified()
    {
        return $this->verified == User::;
    }

   

    public static function generateVerifyToken()
    {
        return Str::random(40);
    }
*/
}
