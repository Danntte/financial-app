<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InventoryImages extends Model
{
    use HasFactory;

    protected $table = 'inventory_images';

    public function media()
    {
        return $this->hasMany(MediaStorage::class);
    }
}
