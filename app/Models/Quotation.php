<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    use HasFactory;
    protected $fillable =[
        'provider_id',
        'product_id',
        'order_id',
        'form_id',
        'sale_price',
        'description_1',
        'description_2',
        'description_3',
        'description_4',
        'description_5',
        'status',
        'media_storage_id',
        
        
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function form()
    {
        return $this->belongsTo(form::class);
    }

    public function mediaStorage()
    {
        return $this->belongsTo(MediaStorage::class);
    }

}
