<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InventoryProduct extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'abstract',
        'description',
        'price',
        'owner',
        'status',
        'category',
        'sale',
        'media_storage_id',
    ];
    /**
     * Get the user associated with the InventoryProduct
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function mediaStorage()
    {
        return $this->belongsToMany(MediaStorage::class);
    }

    
}
