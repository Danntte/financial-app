<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    use HasFactory;

    const PRODUCT_IS_UNASIGNED = 0;
    const PRODUCT_IS_INSURANCE = 1;
    const PRODUCT_IS_FINANCIAL = 2;

    protected $fillable =[
        'name',
        'description',
        'business_type',
      
    ];

   
    public function isUnasigned()
    {
        return $this->status == ProductCategory::PRODUCT_IS_UNASIGNED;
    }
    public function isInsurance()
    {
        return $this->status == ProductCategory::PRODUCT_IS_INSURANCE;
    }
    public function isFinancial()
    {
        return $this->status == ProductCategory::PRODUCT_IS_FINANCIAL;
    }
    public function products()
    {
        return $this->hasMany(Product::class);
    }
    public function orders()
    {
        return $this->hasMany(Order::class);
    }
    
}
