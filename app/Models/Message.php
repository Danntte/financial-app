<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\MessageRoom;
use App\Models\User;

class Message extends Model
{
    use HasFactory;

    /**
     * Get the MessageRoom associated with the Message
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function messageRoom()
    {
        return $this->belongsTo(MessageRoom::class, 'message_rooms_id', 'id');
    }

    /**
     * Get the user associated with the Message
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

}
