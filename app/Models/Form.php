<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    use HasFactory;

    protected $fillable =[
        
        'provider_id',
        'product_id',
        'description'
        
    ];
   
    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    public function mediaStorage()
    {
        return $this->belongsToMany(MediaStorage::class);
    }
    public function quotations()
    {
        return $this->hasMany(Quotation::class);
    }
   

}
