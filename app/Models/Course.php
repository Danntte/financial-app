<?php

namespace App\Models;

use App\Http\Controllers\MediaStorage\MediaStorageController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;

class Course extends Model
{
    use HasFactory;

    const COURSE_IS_VISIBLE = TRUE;
    const COURSE_NOT_VISIBLE = FALSE;

    protected $fillable =[
        'name',
        'slug',
        
        'description',
        'user_id',
        'media_storage_id',
        'course_category_id',
        'visible',
        'rate',
        
        
    ];
    protected $appends = [
        'excerpt',
        'createdAt',
        'image',
    ];

    public function courseCategory()
    {
        return $this->belongsToMany(CourseCategory::class);
    }

    public function lessons()
    {
        return $this->hasMany(Lesson::class);
    }
    public function rates()
    {
        return $this->hasMany(Rate::class);
    }
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function isVisible()
    {
        return $this->visible == Course::COURSE_IS_VISIBLE;
    }

    public function courseImage()
    {
        return $this->hasOne(MediaStorageController::class);
    }
    public function mediaStorage()
    {
        return $this->belongsTo(MediaStorage::class);
    }
    
    public function getExcerptAttribute()
    {
        return substr($this->description, 0, 80) . "...";
    }
    public function getCreatedAtAttribute()
    {
        return Carbon::createFromTimeString($this->attributes['created_at'])->diffForHumans();
    }
    public function getImageAttribute()
    {
        
        return Course::with('mediaStorage');
    }
    
}
