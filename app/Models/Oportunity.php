<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Oportunity extends Model
{
    use HasFactory;
    const Oportunity_IS_OPEN = 'Abierta';
    const Oportunity_IS_CLOSE = 'Cerrada';
    const Oportunity_ON_EVALUATION = 'En revisión ';

    protected $fillable =[
        'name',
        'email',

        'customer_phone',
        'address',
        'date_of_birth',
        'subject',
        'gender',
        'id_type',
        'id_number',
        'message',
        'product_id',
        'client_id',
        'seller_id',  
        'status',
        'opening',
        'clousure',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function order()
    {
        return $this->hasOne(Order::class);
    }

    public function isOpen()
    {
        return $this->status == Oportunity::Oportunity_IS_OPEN;
    }
}
