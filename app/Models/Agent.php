<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agent extends User
{
    use HasFactory;

    CONST USER_IS_AGENT = 1;
    CONST USER_NO_AGENT = 0;

    protected $fillable = [
        'firstname',
        'lastname',
        
        'email',
        'password',
        //'verified',
        //'verification_token',
        //'admin',
        //'agent',
        'user_type',
        'birthday',
        'gender',
        'telephone',
        'mobile_number',
    ];

    public function isAgent()
    {
        return $this->user_type == Agent::USER_IS_AGENT;
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
