<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    const ORDER_IS_OPEN = TRUE;
    const ORDER_IS_CLOSE = FALSE;

    protected $fillable =[
        'amount',
        'category_product_id',
        'product_id',
        'client_id',
        'client_name',
        'seller_id',
        'traking_id',
        'status',
        'sale_price',
        'buy_date',
        'due_date',
        'title_1',
        'title_2',
        'title_3',
        'title_4',
        'title_5',
        'media_storage_id',
        'oportunity_id',
        'email',
        'address',
        'date_of_birth',
        'quotation_title',
        'quotation_date',
        'gender',
        'id_type',
        'id_number',
        'title_insurance_card_1',
        'title_insurance_card_2',
        'title_insurance_card_3',
        'title_insurance_card_4',
        'title_insurance_card_5',
        'description_insurance_card_1',
        'description_insurance_card_2',
        'description_insurance_card_3',
        'description_insurance_card_4',
        'description_insurance_card_5',
        'title_value_insurance_card_1',
        'insured_value',
        'link_payment',
        'buttonStatus',
        'customer_phone'
    ];
    
    public function oportunity()
    {
        return $this->belongsTo(Oportunity::class);
    }
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function productCategory()
    {
        return $this->belongsToMany(ProductCategory::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
    public function agent()
    {
        return $this->belongsTo(Agent::class);
    }

    public function isOpen()
    {
        return $this->status == Order::ORDER_IS_OPEN;
    }
    
    public function mediaStorage()
    {
        return $this->belongsToMany(MediaStorage::class);
    }

    public function quotations()
    {
        return $this->hasMany(Quotation::class);
    }

}
