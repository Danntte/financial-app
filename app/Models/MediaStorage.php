<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Facade\FlareClient\Stacktrace\File;
use Illuminate\Support\Facades\Storage;

class MediaStorage extends Model
{
    use HasFactory;

    protected $table = 'media_storage';

    protected $fillable = [
        'name',
        'display_name',
        'type',
        'description',
        'storage',
    ];

    protected $appends = [
        'nameUrl',
        
    ];
    

    public function storageMedia($media, string $description)
    {
        if( $media->extension() == 'jpg' || $media->extension() == 'jpeg' || $media->extension() == 'png'){
            $folder = 'images';
        }elseif( $media->extension() == 'pdf' || $media->extension() == 'docx' || $media->extension() == 'xlsx' || $media->extension() == 'pptx' ||
                 $media->extension() == 'txt' || $media->extension() == 'doc' || $media->extension() == 'xls' || $media->extension() == 'ppt'){
            $folder = 'documents';
        }elseif( $media->extension() == 'zip' || $media->extension() == '7zip' || $media->extension() == 'rar'){
            $folder = 'compressed';
        }else{
            return $php_errormsg = 'Media Type not supported';
        }
        
        if($media){ 
            $mediaName = $media->getClientOriginalName();
            $mediaType = $media->extension();
            $storage = Storage::disk('public')->put($folder, $media);

            return $this->create([
                "name" => $storage,
                "display_name" => $mediaName,
                "type" => $mediaType,
                "description" => $description,
                "storage" => $folder
            ]);
        }
    }

    
    public function courses()
    {
        return $this->hasMany(Course::class);
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }

    public function getNameUrlAttribute()
    {   
        return Storage::url( $this->name);
    }
    

    public function lessons()
    {
        return $this->belongsToMany(Lesson::class);
    }
    public function forms()
    {
        return $this->belongsToMany(Form::class);
    }
    public function quotations()
    {
        return $this->hasMany(Quotation::class);
    }
    /**
     * Get the user associated with the MediaStorage
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function inventoryProduct()
    {
        return $this->belongsToMany(InventoryProduct::class);
    }

   
    public function provider()
    {
        return $this->hasOne(Provider::class);
    }
    
}
