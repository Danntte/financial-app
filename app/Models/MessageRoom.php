<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Message;

class MessageRoom extends Model
{
    use HasFactory;

    /**
     * Get all of the messages for the MessageRoom
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(Message::class, 'message_rooms_id', 'id');
    }

    public function oldestMessage()
    {
        return $this->hasMany(Message::class, 'message_rooms_id', 'id')->with('user')->latest();
    }
}
