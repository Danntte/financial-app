<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    use HasFactory;

    const PRODUCT_IS_AVAILABLE = TRUE;
    const PRODUCT_IS_NOT_AVAILABLE = FALSE;

    protected $fillable =[
        'name',
        'description',
        'status',
        'product_category_id',
        
        
    ];

    public function productCategory()
    {
        return $this->belongsTo(ProductCategory::class);
    }
   
    public function form()
    {
        return $this->hasMany(Form::class);
    }
    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }
    
    public function oportunities()
    {
        return $this->hasMany(Oportunity::class);
    }

    public function quotations()
    {
        return $this->hasMany(Quotation::class);
    }
    
    public function isAvailable()
    {
        return $this->status == Product::PRODUCT_IS_AVAILABLE;
    }
}
