<?php

namespace App\Http\Controllers\FormProductProvider;

use App\Http\Controllers\Controller;
use App\Models\Form;
use App\Models\MediaStorage;
use App\Models\Product;
use App\Models\Provider;
use Illuminate\Http\Request;
use Inertia\Inertia;

class FormProductProviderAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all();
        $providers = Provider::all();
        return Inertia::render('ProductsProviders/Forms/FormsCreate',[
            'products' => $products,
            'providers' => $providers

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'product_id' => 'required',
           
            'provider_id' => 'required',
            
           

        ]);
       
       $form = Form::create($request->all());
       //sleep(2);
       
       return redirect()->route('formsProductsProviders.edit', $form->id)->with('status', 'Formulario creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $form_ = Form::with('product','provider','mediaStorage')->where('id',$id)->get();
        
        return Inertia::render('ProductsProviders/Forms/FormsEdit',[
            'form_' => $form_[0],
        ]);
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function form_upload_files(Request $request, Form $form)
    {
        $media = $request->media_upload;
        $description = 'Formularios';

        $mediaStorage = new MediaStorage();
        $mediaStorage = $mediaStorage->storageMedia($media, $description);
        $form->mediaStorage()->attach($mediaStorage->id);
        return redirect()->route('formsProductsProviders.edit',$form->id)->with('status', 'archivo cargado');
    }
    
    public function form_delete_media(Request $request, Form $form)
    {
        
        $form->mediaStorage()->detach($request->mediaStorage_id);
        return redirect()->back()->with('status', 'archivo eliminado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $form = Form::with('mediaStorage')->find($id);
        //$form = $form->count();
        $mediaNumber= $form->mediaStorage->count();
        //return $mediaNumber;
         if ($mediaNumber>0){
             return redirect()->back()->with('status', 'No se puede borrar el formulario debido a que tiene un archivos asociados');
         }else{
            
            $form->delete();
            return redirect()->route('productsProviders')->with('status', 'formulario  eliminado');
         }
    }
}
