<?php

namespace App\Http\Controllers\Inventory;

use App\Http\Controllers\Controller;
use App\Models\InventoryProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use App\Models\MediaStorage;
use App\Models\Oportunity;
use PhpParser\Node\Stmt\Return_;

class InventoryController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inventory = InventoryProduct::with('mediaStorage')->latest()->get();
        return Inertia::render('Inventory/Inventory', ['inventory' => $inventory]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return Inertia::render('Inventory/InventoryCreate');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mediaStorage = null;
        
        $inventory = InventoryProduct::create([
            'name' => $request->name,
            'abstract' => $request->abstract,
            'description' => $request->description,
            'price' => $request->price,
            'owner' => Auth::user()->id,
            'sale' => false,
            'status' => $request->status,
            'category' => $request->category,
            //'media_storage_id' => $mediaStorage->id,
        ]

        );
        if($request->hasFile('image_upload')){
            global $mediaStorage;
            $media = $request->image_upload;
            $mediaStorage = new MediaStorage();
            $mediaStorage = $mediaStorage->storageMedia($media, "Image for inventory");
            $inventory->mediaStorage()->attach($mediaStorage->id);
            
        }else {
            global $mediaStorage;
            $mediaStorage = null;
        }
        
        


        
        
        return redirect('/inventoryAdmin');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $inventory = InventoryProduct::with('mediaStorage')->where('id', '=', $id)->get();
        $images = InventoryProduct::where('id', '=', $id)->get();
        $images = $images[0]->inventoryImages;
        return Inertia::render('Inventory/InventoryMain', ['inventory' => $inventory,
                                                           'images' => $images]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function edit($id)
     {   
        $inventory = InventoryProduct::with('mediaStorage')->where('id', '=', $id)->get();
        
        
        
        return Inertia::render('Inventory/InventoryEdit', [
            'inventory' => $inventory[0],
        ]);
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mediaStorage = null;
        
        if($request->hasFile('image_upload')){
            global $mediaStorage;
            $media = $request->image_upload;
            $mediaStorage = new MediaStorage();
            $mediaStorage = $mediaStorage->storageMedia($media, "Image for inventory");
        }else {
            global $mediaStorage;
            $mediaStorage = null;
        }
        
        InventoryProduct::where("id", $id)->update([
            'name' => $request->name,
            'abstract' => $request->abstract,
            'description' => $request->description,
            'price' => $request->price,
            'owner' => Auth::user()->id,
            'status' => $request->status,
            'category' => $request->category,
            //'media_storage_id' => $mediaStorage->id,
        ]
        );
        
        return redirect('/inventoryAdmin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {

    }

    
    // temporary inventory page controllers

    public function inventoryAdmin()
    {
        $inventory = InventoryProduct::where('owner', '=', Auth::user()->id)->with('mediaStorage')->latest()->get();
        return Inertia::render('Inventory/InventoryAdmin', ['inventory' => $inventory]);
    }

 
    public function createOpportunity(Request $request)
    {

        if($request->category == 'Inmuebles'){
            $product_id_inventory = 17;
        }else if ($request->category == 'Vehiculos'){
            $product_id_inventory = 20;
        }

        Oportunity::create([
            'email' => Auth::user()->email,
            'name' => Auth::user()->name,
            'client_id' => Auth::user()->id,
            'seller_id' => $request->seller_id,
            'status' => "Abierta",
            'opening' => date('Y-m-d H:i:s'),
            'product_id' => $product_id_inventory,

        ], 200);

        return redirect()->back()->with('status','Solicitud enviada.'); 
    }

    public function inventory_upload_files(Request $request, $id)
    {
       
        $media = $request->media_upload;
        $description = 'Image for inventory';
        
        $mediaStorage = new MediaStorage();
        $mediaStorage = $mediaStorage->storageMedia($media, $description);
        $inventory= InventoryProduct::findOrFail($id);
        $inventory->mediaStorage()->attach($mediaStorage->id);
        return redirect()->route('inventory.edit',$inventory->id)->with('status', 'imagen cargada');

    }

    public function inventory_delete_media(Request $request, $id)
    {
        $inventory= InventoryProduct::findOrFail($id);
        $inventory->mediaStorage()->detach($request->mediaStorage_id);
        return redirect()->back()->with('status', 'Imagen eliminada');
    }

}
