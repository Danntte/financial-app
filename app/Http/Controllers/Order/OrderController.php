<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Quotation;
use Illuminate\Http\Request;
use Inertia\Inertia;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
          //
          return response(Order::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //
         return response(Order::create([
            
            'product_id' => $request->product_id,
            'client_id' => $request->client_id,
            'seller_id' => $request->seller_id,
            'traking_id' => $request->traking_id,
            'status' => $request->status,
            'buy_date' => $request->buy_date,
            'due_date' => $request->due_date,

        ]), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return response(Order::findOrFail($id)->where("id", $id) -> get(), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
        $order->fill($order->only([
            'product_id',
            'client_id',
            'seller_id',
            'traking_id',
            'status',
            'sale_price',
            'buy_date',
            'due_date',
            'oportunity_id',
        ]));

        if ($order->isClean()){// si no ha cambiado da un error
            return Response('debe especificar al menos un valor diferente para actualizar',422);
             }
            $order->save(); // se guardan las modificaciones
    
              return Response($order,200);
        
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function orderQuotation($id)
    {
        $orderQuotation = Order::with('quotations.form.provider')->where("id", $id) -> get();
        
        return Inertia::render('Crm/Quotations',[
            'orderQuotation' => $orderQuotation[0],
        ]);
    }
}
