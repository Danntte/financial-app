<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Models\Oportunity;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

use function GuzzleHttp\Promise\queue;

class OrderPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->all();
        $request->validate([
            'client_name' => 'required',
            'email' => 'required',
            'address' => 'required',
            'date_of_birth' => 'required',
            'gender' => 'required',
            'id_number' => 'required',
            'id_type' => 'required',
            'quotation_title' => 'required',
        ]);
        $order = Order::create($request->all());
        
        $order->update(['status' => 'En espera de cotización',]);
        $data = $request->all();
        $opportunity = Oportunity::create([
            'name' => $data["client_name"],
            'email' => $data["email"],
            'product_id' => $data["product_id"],



    ]);
        //sleep(2);
        
        $order->update(['oportunity_id' => $opportunity->id]);
        $user_type = auth()->user()->user_type;
        if ($user_type==1){
            return redirect()->route('order.edit', $order->id)->with('status', 'orden creada');
        }elseif ($user_type==2){
            return redirect()->route('order.editRol3', $order->id)->with('status', 'orden creada');
        }
        
        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $seller = auth()->user()->id;
        $order =  Order::with('quotations.form.provider')->findOrFail($id);
        $order->update(['seller_id' => $seller]);

        $categories = ProductCategory::with('products')->get();//with('products')->latest()->get();
        //return $categories;
        $products = Product::with('productCategory','form')->get();
        $selectedProducts = $order->quotations;
        $orders = Order::latest()->get();
       
        return Inertia::render('Dashboard2', [            
            'order'=> $order,
            'products' => $products,
            'categories' => $categories,
            'selectedProducts' => $selectedProducts,
            'orders' => $orders
        ]);
    }
    public function editRol3($id)
    {
        //$seller = auth()->user()->id;
        $order =  Order::with('quotations.form.provider')->findOrFail($id);
        //$order->update(['seller_id' => $seller]);

        $categories = ProductCategory::with('products')->get();//with('products')->latest()->get();
        //return $categories;
        $products = Product::with('productCategory','form')->get();
        $selectedProducts = $order->quotations;
        $orders = Order::latest()->get();
       
        return Inertia::render('Dashboard2', [            
            'order'=> $order,
            'products' => $products,
            'categories' => $categories,
            'selectedProducts' => $selectedProducts,
            'orders' => $orders
        ]);
    }

    public function submitedOrder(Order $order)
    {
        //$order->update(['status'=>'Cotización entregada']);
        $user_type = auth()->user()->user_type;
        if ($user_type==1){
            return redirect()->route('order.edit', $order->id)->with('status', 'Cotización modificada');
        }elseif ($user_type==2){
            return redirect()->route('order.editRol3', $order->id)->with('status', 'Cotización modificada');
        }
        //return redirect()->route('order.edit', $order->id);

    }

    public function goToOrder(Order $order)
    {
        //$order->update(['status'=>'Cotización entregada']);
        $user_type = auth()->user()->user_type;
        if ($user_type==1){
            return redirect()->route('order.edit', $order->id);
        }elseif ($user_type==2){
            return redirect()->route('order.editRol3', $order->id);
        }
        //return redirect()->route('order.edit', $order->id);

    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        
        $request->validate([
            'client_name' => 'required',
            'email' => 'required',
            'address' => 'required',
            'date_of_birth' => 'required',
            //'quotation_title' => 'required',
            'gender' => 'required',
            'id_number' => 'required',
            'id_type' => 'required'
            
            
        ]);
        
        $order->update($request->all());
        $user_type = auth()->user()->user_type;
        if ($user_type==1){
            return redirect()->route('order.edit', $order->id)->with('status', 'orden editada');
        }elseif ($user_type==2){
            return redirect()->route('order.editRol3', $order->id)->with('status', 'orden editada');
        }
        
        
    }
    public function update_titles(Request $request, Order $order)
    {
        $data=$request->all();
        $data;
        $order->update([
            'title_1' => $data["title_1"],
            'title_2' => $data["title_2"],
            'title_3' => $data["title_3"],
            'title_4' => $data["title_4"],
            'title_5' => $data["title_5"],
        ]);
        
        return  redirect()->back()->with('status', 'Titulos actualizados');
    }
    public function update_headers(Request $request, Order $order)
    {
        //return $request;
        $data=$request->all();
        
        //return $data['buttonStatus'];
        $order->update($data);
        
        if ($data['buttonStatus']<1)
        {
            $order->update(['buttonStatus' => 1]);
        }
        
        
        $user_type = auth()->user()->user_type;
        if ($user_type==1){
            return redirect()->route('order.edit', $order->id)->with('status', 'Encabezados actualizados');
        }elseif ($user_type==2){
            return redirect()->route('order.editRol3', $order->id)->with('status', 'Encabezados actualizados');
        }
        //return  redirect()->back()->with('status', 'Encabezados actualizados');
    }

    public function statusToStandby(Order $order)
    {

        $order->update(['status' => 'Orden en espera']);
        $user_type = auth()->user()->user_type;
        if ($user_type==1){
            return redirect()->route('order.edit', $order->id);
        }elseif ($user_type==2){
            return redirect()->route('order.editRol3', $order->id);
        }
        //return redirect()->back();
    }

    public function statusToQuotationSubmited(Order $order)
    {
        $order->update(['status' => 'Cotización entregada']);
        $user_type = auth()->user()->user_type;
        if ($user_type==1){
            return redirect()->route('order.edit', $order->id);
        }elseif ($user_type==2){
            return redirect()->route('order.editRol3', $order->id);
        }
        //return redirect()->back();
    }

    public function statusToApproved(Order $order)
    {
        $order->update(['status' => 'Aprobada o emitida']);
        $user_type = auth()->user()->user_type;
        if ($user_type==1){
            return redirect()->route('order.edit', $order->id);
        }elseif ($user_type==2){
            return redirect()->route('order.editRol3', $order->id);
        }
        //return redirect()->back();
    }

    public function statusToCancelled(Order $order)
    {
        $order->update(['status' => 'Orden cancelada']);
        $user_type = auth()->user()->user_type;
        if ($user_type==1){
            return redirect()->route('order.edit', $order->id);
        }elseif ($user_type==2){
            return redirect()->route('order.editRol3', $order->id);
        }
        //return redirect()->back();
    }

    public function statusToFinished(Order $order)
    {
        $order->update(['status' => 'Orden finalizada']);
        $user_type = auth()->user()->user_type;
        if ($user_type==1){
            return redirect()->route('order.edit', $order->id);
        }elseif ($user_type==2){
            return redirect()->route('order.editRol3', $order->id);
        }
        //return redirect()->back();
    }

    public function goToLink_payment(Order $order)
    {
        $link = $order->link_payment;
        //return  $link;
        return response('', 409)->header('X-Inertia-Location', $link);
    }
    public function updatePaymentLink(Request $request, Order $order)
    {
        $data =  $request->all();
        $order->update(['link_payment' => $data["link_payment"]]);
        $user_type = auth()->user()->user_type;
        if ($user_type==1){
            return redirect()->route('order.edit', $order->id);
        }elseif ($user_type==2){
            return redirect()->route('order.editRol3', $order->id);
        }
        //return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
