<?php

namespace App\Http\Controllers\Library;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Lesson;
use App\Models\MediaStorage;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

class LibraryPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::latest()->with('user','mediaStorage')->get();
        
        //return $courses[0];
       
        return Inertia::render('Library/Index', [
            'courses' => $courses,

            
                
                
        ]);
        
    }

    


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        return Inertia::render('Library/Course', compact('course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display a course.
     *
     * @return \Illuminate\Http\Response
     */
    public function course(Course $course)
    {
        //return $courseCreator=Course::with('user')->get()[0]->user->name;
        $course =  Course::with('mediaStorage')->findOrFail($course->id);
        //return $course;
        
        return Inertia::render('Library/Course', 
        ['courses' => Course::latest()->with('user','mediaStorage')->get(),
         'course' => $course,
         'courseCreator' => $course->user,
         'lessons' => $course->lessons
        ]);

    }
    public function lesson(Lesson $lesson)
    {
        //return $lesson;
        $lesson = Lesson::with('mediaStorage')->findOrFail($lesson->id);
        $lessons= Lesson::latest()->with('course','mediaStorage')->where('course_id',$lesson->course->id)->get();
        $courseCreator= $lesson->course->user;
        $course=$lesson->course;
        return Inertia::render('Library/Lesson', 
        ['lessons' => $lessons,
         'lesson' => $lesson,
         'courseCreator' =>  $courseCreator,
         'course'=> $course

        ]);
        

    }
}
