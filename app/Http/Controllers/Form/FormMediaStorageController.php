<?php

namespace App\Http\Controllers\Form;

use App\Http\Controllers\Controller;
use App\Models\Form;
use App\Models\MediaStorage;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Provider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;
use Illuminate\Support\Str;

class FormMediaStorageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        /**
         * this controller show the Descargas.vue with mediaStorage associated to a form
         */
        $categories = ProductCategory::with('products')->get();//with('products')->latest()->get();
        //return $categories;
        $products = Product::with('productCategory','form')->get();
        $providers = Provider::all();
        $forms = Form::with('product','provider','mediaStorage')->get();
        $form_ = Form::with('product','provider','mediaStorage')->where('id',$id)->get();
        $mediaStorage = $form_[0]->mediaStorage;
        
        
        return Inertia::render('Crm/Descargas',[
            'mediaStorage' => $mediaStorage,
            'products' => $products,
            'categories' => $categories,
            'form_' => $form_,
            'providers' => $providers,
            'forms' => $forms
        ]);
        
    }

    /**
     * download
     */
    public function downloadFile($id)
    {
        /**
         * this controller download files in the storage media
         */
        $mediaStorage = MediaStorage::find($id);
        
        $subjectCode = $mediaStorage->name;
        $subjectName = $mediaStorage->display_name;
        redirect(Storage::url($mediaStorage->name)); 
        return response()->download(public_path().Storage::url($mediaStorage->name), $subjectName);
        //return response()->download(storage_path().Storage::url($mediaStorage->name), $subjectName);
    }
    public function showFiles()
    {
        /**
         * this controller show de Descargas view but withohut forms list 
         */

        $categories = ProductCategory::with('products')->get();//with('products')->latest()->get();
        //return $categories;
        $products = Product::with('productCategory','form')->get();
        $providers = Provider::all();
        $forms = Form::all();

        

        return Inertia::render('Crm/Descargas',[
            'products' => $products,
            'categories' => $categories,
            
            'providers' => $providers,
            'forms' => $forms
        ]);
        
    }

    public function downloads_orders(Order $order)
    {
        $order_ = Order::with('quotations.form.mediaStorage','quotations.form.product','quotations.form.provider')->where('id',$order->id)->get();
        $quotations = $order_[0]->quotations;
        $quotations = $quotations->where('status', 1);
        return Inertia::render('Crm/DescargasRoll1',[
            
            'order' =>$order_,
            'quotations' => $quotations,
        ]);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function show(Form $form)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function edit(Form $form)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Form $form)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function destroy(Form $form)
    {
        //
    }
}
