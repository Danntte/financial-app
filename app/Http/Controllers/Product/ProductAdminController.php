<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Models\Form;
use App\Models\Oportunity;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ProductAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('ProductsProviders');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $productCategories = ProductCategory::all();
        return Inertia::render('ProductsProviders/Products/ProductsCreate',[
            'productCategories' => $productCategories,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'required',
            
            'description' => 'required',
            
            'product_category_id' => 'required',

            'status' => 'required',
            

        ]);
        $product = Product::create($request->all());
        return redirect()->route('products.edit', $product->id)->with('status', 'Producto creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $ProductCategories = ProductCategory::all();
        return Inertia::render('ProductsProviders/Products/ProductsEdit',[
            'product' => $product,
            'productCategories' => $ProductCategories

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'name'=> 'required',
            'description'=> 'required',
            'status'=> 'required',
            'product_category_id'=> 'required',
            
        ]);
        $product->update($request->all());
        return redirect()->route('productsProviders')->with('status','Producto actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        
        
        //
        // $forms = $product->form;
        // //return $forms;
        
        // foreach ($forms as $form){
            
        //     foreach ($form->mediaStorage as $media){
        //         $form->mediaStorage()->detach($media);
        //         //$media->delete();
        //     }
        //     foreach ($form->quotations as $quotation){
        //         //$form->quotations()->detach($quotation);
        //         $quotation->delete();
        //     }
            
        //     $form->delete();

        // }
        
        // $orders = $product->orders;
        
        //  foreach ($orders as $order){
            
             
        //      $order->delete();

        //  }
         
        // $opportunities = $product->oportunities;
        // foreach ($opportunities as $opportunity){
           
        //      $order = $opportunity->orders;
             
        //      foreach ($order->mediaStorage as $media){
        //          $order->mediaStorage()->detach($media);
        //          //$media->delete();
        //      }
        //      //$order->mediaStorage()->attach($mediaStorage);
        
        //      foreach ($order->quotations as $quotation){
        //          $quotation->delete();
               
        //      }
            
        //     $order -> delete();
            
        //     $opportunity->delete();
            

        // }
        $ordersNumber =  Order::where('product_id',$product->id)->count();
        $opportunitiesNumber =  Oportunity::where('product_id',$product->id)->count();
        $formNumber = Form::where('product_id',$product->id)->count();
        if ($ordersNumber>0 || $opportunitiesNumber> 0 || $formNumber>0){
            return redirect()->back()->with('status', 'No se puede borrar el prducto debido a que tienen ordenes, oportunidades o formularios asociados');
        }else{
            
            $product->delete();
            return redirect()->route('productsProviders')->with('status', 'Producto  eliminado');
        }
        
    }
}
