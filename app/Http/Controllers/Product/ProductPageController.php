<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ProductPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //return Product::where('id',$id)->get();
        $categories = ProductCategory::with('products')->get();//with('products')->latest()->get();
        //return $categories;
        $products = Product::with('productCategory')->get();
        $product = Product::findOrFail($id);
        //return $product->product_category_id;

        switch ($product->product_category_id) {
            case 1:
                return Inertia::render('Insurances/SegurosVida',[
                
                    'products' => $products,
                    'categories' => $categories,
                    'product' => $product,

                ]);
            case 2:
                return Inertia::render('Insurances/SegurosVehiculos',[
                
                    'products' => $products,
                    'categories' => $categories,
                    'product' => $product,

                ]);
            case 3:
                return Inertia::render('Insurances/SegurosPersonal',[
                
                    'products' => $products,
                    'categories' => $categories,
                    'product' => $product,

                ]);
            case 4:
                return Inertia::render('Insurances/SegurosHogar',[
                
                    'products' => $products,
                    'categories' => $categories,
                    'product' => $product,

                ]);
            case 5:
                return Inertia::render('Insurances/CreditosHipotecareos',[
                
                    'products' => $products,
                    'categories' => $categories,
                    'product' => $product,

                ]);

            case 6:
                return Inertia::render('Insurances/CreditosConsumo',[
                
                    'products' => $products,
                    'categories' => $categories,
                    'product' => $product,

                ]);

            case 7:
                return Inertia::render('Insurances/CreditosVehículos',[
                
                    'products' => $products,
                    'categories' => $categories,
                    'product' => $product,

                ]);
            
            default:
                return Inertia::render('Insurances/SegurosVida',[
                    
                    'products' => $products,
                    'categories' => $categories,
                    'product' => $product,

                ]);
                
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
