<?php

namespace App\Http\Controllers\Course;

use App\Http\Controllers\Controller;
use App\Models\CourseCategory;
use Illuminate\Http\Request;

class CourseCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(CourseCategory::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response(CourseCategory::create([
           
            'name' => $request->name,
            'description' => $request->description,
            
                ])
            , 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(CourseCategory::findOrFail($id)->where("id", $id) -> get(), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CourseCategory $courseCategory)
    {
        $courseCategory->fill($request->only([

            'name',            
            'description',
            

            ]));

            // ahora verificamos si cambio algo de lo que se tenia anteriormente
            // isDirty verifica si ha cambiado()  e isClean() hace lo contrario
           if ($courseCategory->isClean()){// si no ha cambiado da un error
           return Response('debe especificar al menos un valor diferente para actualizar',422);
            }
           $courseCategory->save(); // se guardan las modificaciones
   
             return Response($courseCategory,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CourseCategory $courseCategory)
    {
        $courseCategory->delete();
        return response($courseCategory, 200);
    }
}
