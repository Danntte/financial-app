<?php

namespace App\Http\Controllers\Course;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\MediaStorage;
use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CoursePageAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $courses = Course::with('mediaStorage')->latest()->get();
        return Inertia::render('Courses/IndexAdmin', [
            'courses' => $courses
                /*->where('name', 'LIKE', "%$request->q%")*/
                
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = CourseCategory::all();
        $admins = Admin::where('user_type','=','2')->get();
        return Inertia::render('Courses/CreateAdmin',[
            'categories' => $categories,      
            'admins' => $admins , 
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request;
        if($request->hasFile('thumbnail')){
            $filename = $request->thumbnail->getClientOriginalName();
            info($filename); //log
        }
        
        $request->validate([
            'name' => 'required',
            'slug' => 'required',
            'description' => 'required',
            //'media_storage_id' => 'required',
            'thumbnail' => 'required',
            'visible' => 'required',
            'user_id' => 'required',
            'course_category_id' => 'required',

        ]);
        $course = Course::create($request->all());

        $media = $request->thumbnail;
        $description = $request->name;

        $mediaStorage = new MediaStorage();
        $mediaStorage = $mediaStorage->storageMedia($media, $description);
        $course->update(['media_storage_id' => $mediaStorage->id ]);
        return redirect()->route('courses.edit', $course->id)->with('status', 'Curso creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        //return $course->lessons;
        $course =  Course::with('mediaStorage')->findOrFail($course->id);
        
        return Inertia::render('Courses/ShowAdmin', [            
            'course'=> $course,
            'lessons' => $course->lessons,        
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        $course =  Course::with('mediaStorage')->findOrFail($course->id);
        $categories = CourseCategory::all();
        $admins = Admin::where('user_type','=','2')->get();
       
        return Inertia::render('Courses/EditAdmin', [            
            'course'=> $course,
            'lessons' => $course->lessons,
            'categories' => $categories,      
            'admins' => $admins  
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {

        //return $request;
        $request->validate([
            'name'=> 'required',
            'slug'=> 'required',
            'description'=> 'required',
            'media_storage_id' => 'required',
            'visible'=> 'required',
            'user_id'=> 'required',
            'course_category_id'=> 'required',
            
        ]);
        
        if($request->hasFile('thumbnail')){
            //$data = $request->all();
            //$filename = $request->thumbnail->getClientOriginalName();
            //info($filename); //log
            $media = $request->thumbnail;
            $description = $request->name;

            $mediaStorage = new MediaStorage();
            $mediaStorage = $mediaStorage->storageMedia($media, $description);
            $course->update($request->all());
            $course->update(['media_storage_id' => $mediaStorage->id ]);
           // return 1;
        }
        else{

        $course->update($request->all());
    }

        return redirect()->route('courses.index')->with('status', 'Curso actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        
        $course->delete();
        return redirect()->route('courses.index')->with('status', 'Curso eliminado');
    }
}
