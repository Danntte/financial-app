<?php

namespace App\Http\Controllers\Message;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MessageRoom;
use App\Models\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Inertia\Inertia;

class MessageController extends Controller
{
    public function roomIndex($roomId){
        return Inertia::render('Messages/messageIndex', [
            'room' => $roomId,
            'userId' => Auth::user()->id,
        ]);
    }

    public function roomsVue(){
        return Inertia::render('Messages/messageContainer');
    }

    public function rooms(){
        return MessageRoom::with('oldestMessage')->limit(5)->latest('updated_at')->get();
    }

    public function messages(Request $request, $roomId){
        return Message::where('message_rooms_id', '=', $roomId)
        ->with('user')
        ->orderBy('created_at', 'ASC')
        ->get();
    }

    public function newMessage(Request $request, $roomId){
        $newMessage = new Message();
        $newMessage->user_id = Auth::id();
        $newMessage->message_rooms_id = $roomId;
        $newMessage->message = $request->message;
        $newMessage->save();

        MessageRoom::where('id', '=', $roomId)->update(['updated_at' => date("Y-m-d H:i:s")]);
        return $newMessage;
    }
}
