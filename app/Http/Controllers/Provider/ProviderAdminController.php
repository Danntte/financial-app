<?php

namespace App\Http\Controllers\Provider;

use App\Http\Controllers\Controller;
use App\Models\Form;
use App\Models\MediaStorage;
use App\Models\Provider;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ProviderAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('ProductsProviders/Providers/ProvidersCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('thumbnail')){
            $filename = $request->thumbnail->getClientOriginalName();
            info($filename); //log
        }
        $request->validate([
            'name'=> 'required',
            'description'=> 'required',
            'status'=> 'required',
            'description'=> 'required',
           
            'phone' => 'required',
            'email' => 'required',
                   
            
        ]);
        $provider = Provider::create($request->all());
        
        $media = $request->thumbnail;
        $description = $request->name;

        $mediaStorage = new MediaStorage();
        $mediaStorage = $mediaStorage->storageMedia($media, $description);
        $provider->update(['media_storage_id' => $mediaStorage->id ]);
        return redirect()->route('providers.edit', $provider->id)->with('status', 'Proveedor creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Provider $provider)
    {
        $provider = Provider::with('mediaStorage')->find($provider->id);
        return Inertia::render('ProductsProviders/Providers/ProvidersEdit',[
            'provider' => $provider,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Provider $provider)
    {
        //
        $request->validate([
            'name'=> 'required',
            'description'=> 'required',
            'status'=> 'required',
            'description'=> 'required',
           
            'phone' => 'required',
            'email' => 'required',
                   
            
        ]);

        if($request->hasFile('thumbnail')){
            //$data = $request->all();
            //$filename = $request->thumbnail->getClientOriginalName();
            //info($filename); //log
            $media = $request->thumbnail;
            $description = $request->name;

            $mediaStorage = new MediaStorage();
            $mediaStorage = $mediaStorage->storageMedia($media, $description);
            $provider->update($request->all());
            $provider->update(['media_storage_id' => $mediaStorage->id ]);
           // return 1;
        }
        else{

        $provider->update($request->all());
    }

        
        return redirect()->route('productsProviders')->with('status','Proveedor actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Provider $provider)
    {
        //
        //$ordersNumber =  Order::where('product_id',$product->id)->count();
        //$opportunitiesNumber =  Oportunity::where('product_id',$product->id)->count();
        $formNumber = Form::where('provider_id',$provider->id)->count();
        if ($formNumber>0){
            return redirect()->back()->with('status', 'No se puede borrar el proveedor debido a que tiene un formulario asociado');
        }else{
            
            $provider->delete();
            return redirect()->route('productsProviders')->with('status', 'Proveedor  eliminado');
        }
               
       
    }
}
