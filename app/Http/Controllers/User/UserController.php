<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
        return response(['data' => $user] , 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        

        $request->validate([
        

            // firstname: '',
            //         lastname: '',//this contains infotmation that is going to be display in a box
            //         //media_storage_id: '',
            //         email: '', 
            //         password: '',// in this case it should be empty 
            //         user_id: '',
            //         gender: '',
            //         telephone:'',
            //         mobile_number:'',
                'firstname' => 'required',
                'lastname' => 'required',
                'email' => 'required',
                'password' => 'required',
                'user_type'=> 'required',
                'gender'=> 'required',
                'telephone' => 'required',
                'mobile_number' => 'required',
                //'birthday' => 'required',    
                
                    ]);
        //return $request->all();

        $course = User::create($request->all());
        return redirect()->back()->with('status', 'Usuario creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(User::findOrFail($id)->where("id", $id) -> get(), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        User::where('id', $id)->update([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'birthday' => $request->birthday,
            'telephone' => $request->telephone,
            'mobile_number' => $request->mobile_number,
                ]);

        return response('Ok', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        //User::destroy($id);
        $user->delete();
        return response($user, 200);
    }
}
