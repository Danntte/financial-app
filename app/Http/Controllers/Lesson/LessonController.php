<?php

namespace App\Http\Controllers\Lesson;

use App\Http\Controllers\Controller;
use App\Models\Lesson;
use Illuminate\Http\Request;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Lesson::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response(Lesson::create([
            
            'name' => $request->name,
            'visible' => $request->visible,
            'description' => $request->description,
            'video' => $request->video,
            'course_id' => $request->course_id,
            
            

        ]), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Lesson::findOrFail($id)->where("id", $id) -> get(), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lesson $lesson)
    {
        $lesson->fill($request->only([

            'name' ,
            'visible',
            'description', 
            'course_id',
         ]));

         // ahora verificamos si cambio algo de lo que se tenia anteriormente
         // isDirty verifica si ha cambiado()  e isClean() hace lo contrario
        if ($lesson->isClean()){// si no ha cambiado da un error
        return Response('debe especificar al menos un valor diferente para actualizar',422);
         }
        $lesson->save(); // se guardan las modificaciones

          return Response($lesson,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lesson $lesson)
    {
        $lesson->delete();
        return response($lesson, 200);
    }
}
