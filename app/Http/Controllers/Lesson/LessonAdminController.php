<?php

namespace App\Http\Controllers\Lesson;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Lesson;
use App\Models\MediaStorage;
use Illuminate\Http\Request;
use Inertia\Inertia;

class LessonAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_lesson(Course $course)
    {
        //return $course;
        //return Course::all()->where('id','=',$course->id);
        
        return Inertia::render('Courses/Lessons/Create', [
        'course' => $course->id,
        'courseName' => $course->name,
        
       ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'visible' => 'required',
            'description' => 'required',
            //'video' => 'required',            
            'course_id' => 'required',
            

        ]);
        $lesson = Lesson::create($request->all());
        $media = $request->media_upload;
        $description = 'PDF';

        $mediaStorage = new MediaStorage();
        $mediaStorage = $mediaStorage->storageMedia($media, $description);
        $lesson->mediaStorage()->attach($mediaStorage->id);
        return redirect()->route('courses.edit', $lesson->course->id)->with('status', 'Lección creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Lesson $lesson)
    {
        
        $lesson =  Lesson::with('mediaStorage')->findOrFail($lesson->id);
        
        $course = Course::findOrFail($lesson->course_id); //$course = Course::where('id',$lesson->course_id)->get();
        $course = $course->name; //$course[0]->name;
        

        return Inertia::render('Courses/Lessons/Edit', [            
            'lesson'=> $lesson,
            'course' => $course,
            
            
                    
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lesson $lesson)
    {
        //return $request;
        $request->validate([
            'name' => 'required',
            'visible' => 'required',
            'description' => 'required',
            //'video' => 'required',            
            'course_id' => 'required',
            
        ]);

        $lesson->update($request->all());

        return redirect()->route('courses.edit',$lesson->course_id)->with('status', 'Lección actualizada');
    }
    public function lesson_upload_files(Request $request, Lesson $lesson)
    {
        $media = $request->media_upload;
        $description = 'PDF';

        $mediaStorage = new MediaStorage();
        $mediaStorage = $mediaStorage->storageMedia($media, $description);
        $lesson->mediaStorage()->attach($mediaStorage->id);
        return redirect()->route('lesson.edit',$lesson->id)->with('status', 'PDF cargado');
    }
    
    public function lesson_delete_media(Request $request, Lesson $lesson)
    {
        
        $lesson->mediaStorage()->detach($request->mediaStorage_id);
        return redirect()->back()->with('status', 'archivo eliminado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lesson $lesson)
    {
        $forums = $lesson->forums;
        
        foreach ($forums as $forum){
            $forum->delete();
        }       
        $lesson->delete();
        return redirect()->route('courses.edit',$lesson->course_id)->with('status', 'Lección eliminada');
    }
}
