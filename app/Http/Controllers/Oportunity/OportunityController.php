<?php

namespace App\Http\Controllers\Oportunity;

use App\Http\Controllers\Controller;
use App\Models\Oportunity;
use Illuminate\Http\Request;

class OportunityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response(Oportunity::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return response(Oportunity::create([
            'name' => $request->name,
            'product_id' => $request->product_id,
            'client_id' => $request->client_id,
            'seller_id' => $request->seller_id,
            'status' => $request->status,
            'opening' => $request->buy_date,
            'clousure' => $request->due_date,

        ]), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return response(Oportunity::findOrFail($id)->where("id", $id) -> get(), 200);
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Oportunity $oportunity)
    {
        //
          //
          $oportunity->fill($oportunity->only([
            'product_id',
            'client_id',
            'seller_id',
            'status',
            'opening',
            'clousure',
        ]));

        if ($oportunity->isClean()){// si no ha cambiado da un error
            return Response('debe especificar al menos un valor diferente para actualizar',422);
             }
            $oportunity->save(); // se guardan las modificaciones
    
            return Response($oportunity,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
