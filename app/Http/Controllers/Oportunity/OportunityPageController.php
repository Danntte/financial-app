<?php

namespace App\Http\Controllers\Oportunity;

use App\Http\Controllers\Controller;



use App\Models\Oportunity;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;

class OportunityPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $opportunities = Oportunity::with('product')->latest()->get();
        
          return Inertia::render('Crm/OpportunitiesRoll2',[
              'opportunities' => $opportunities,
            
          ]);
        
    }

    public function indexAdmin()
    {
          $opportunities = Oportunity::with('product')->latest()->get();
        
          return Inertia::render('Crm/OpportunitiesRoll3',[
              'opportunities' => $opportunities,
            
          ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'customer_phone' => 'required',
            'email' => 'required',
            'product_id' => 'required',
            'message' => 'required'

        ]);
        $opportunities = Oportunity::create($request->all());
        return redirect()->route('index')->with('status', 'Gracias por preferirnos, enseguida te llamamos.');
    }

    public function orderFromOpportunities(Request $request, Oportunity $oportunity)
    {
        //return  $oportunity;
        //$request->validate([
        
        $data =  $request->all();
        $request->validate([
            'client_name' => 'required',
            'email' => 'required',
            'customer_phone' => 'required',
            'address' => 'required',
            'date_of_birth' => 'required',
            'quotation_title' => 'required',
            'gender' => 'required',
            'id_number' => 'required',
            'id_type' => 'required',
            



        ]);
        

        $product = Product::where('name', $data["quotation_title"])->get();
        $data["product_id"] = $product[0]->id;
        
        $oportunity->update($data);
        $oportunity->update([
            'name' => $data['client_name'],

        ]);
        //return $oportunity;
        $order = Order::create($data);
        $order->update(['oportunity_id' => $oportunity->id]);
       
        
        
        

        
        $user_type = auth()->user()->user_type;
        if ($user_type==1){
            return redirect()->route('order.edit', $order->id)->with('status', 'orden creada');
        }elseif ($user_type==2){
            return redirect()->route('order.editRol3', $order->id)->with('status', 'orden creada');
        }
        //return redirect()->route('order.edit', $order->id)->with('status', 'orden creada');
        // return Inertia::render('Dashboard2', [            
        //     'order'=> $order,
        //     'products' => $products,
        //     'categories' => $categories,
        //     'selectedProducts' => $selectedProducts,
        //     'orders' => $orders
        // ]);
    }

    public function updateOpportunityAndOrder(Request $request, Oportunity $oportunity)
    {
        $data =  $request->all();
        $request->validate([
            'client_name' => 'required',
            'email' => 'required',
            'customer_phone' => 'required',
            'address' => 'required',
            'date_of_birth' => 'required',
            'quotation_title' => 'required',
            'gender' => 'required',
            'id_number' => 'required',
            'id_type' => 'required',
        ]);
        
        
       if($data["product_id"] == "" || $data["product_id"] == null){
        //return $data;
        $product = Product::where('name', $data["quotation_title"])->get();
        $data["product_id"] = $product[0]->id;
       }else{
        $product = Product::where('id', $data["product_id"])->get();
        $data["quotation_title"] = $product[0]->name;
       }
        
        $oportunity->update($data);
        $oportunity->update([
            'name' => $data['client_name'],

        ]);
        
        //return $data;
        $oportunity = Oportunity::with('order')->findOrFail($oportunity->id);
        
        if($oportunity->order != null){
            
            $order=$oportunity->order;
            
            $order->update($data);
            
            $order->update(['quotation_title' => $data["quotation_title"]]);
        }
            
        return redirect()->back()->with('status', 'Oportudinad actualizada');
        
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function temporalEdit()
    // {
    //     return Inertia::render('Crm/OpportunitiesEdit.vue');
    // }
    public function edit($id)
    {
        //
        $categories = ProductCategory::with('products')->get();
        $products = Product::with('productCategory','form')->get();
        $sellers = User::where('user_type', 1)->get();
        
        $opportunity =  Oportunity::with('product','order')->where('id',$id)->get();
        return Inertia::render('Crm/OpportunitiesEdit.vue',[
            'opportunity' => $opportunity[0],
            'categories' => $categories,
            'products' => $products,
            'sellers' => $sellers


        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
