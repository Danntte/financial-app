<?php

namespace App\Http\Controllers\Oportunity;

use App\Http\Controllers\Controller;
use App\Models\Oportunity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\OpportunityMailable;
use App\Models\Order;
use App\Models\Quotation;
use Inertia\Inertia;

class FormsController extends Controller
{
    

   public function index()
   {
       
   }

   

   public function edit($id)
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $email = new OpportunityMailable;
        Mail::to('admin@test.com')->send($email);
        $request->validate([
            'name' => 'required',
            'customer_phone' => 'required',
            'email' => 'required',
            'product_id' => 'required',
            'message' => 'required'

        ]);
        
        $opportunities = Oportunity::create($request->all());
        return redirect()->route('index')->with('status', 'Gracias por preferirnos, enseguida te llamamos.');
    }

    public function storeFromOrder(Request $request, Order $order)
    {
        
        //return $request;
        $request->validate([
             'name' => 'required',
            
             'email' => 'required',
             'product_id' => 'required',
            

         ]);
        
        $opportunity = Oportunity::create($request->all());
        //sleep(2);
        $order->update(['oportunity_id' => $opportunity->id]);
        return redirect()->back();
        /*$order->oportunity_id = ;*/
        
    }
    

   
    public function destroy($id)
    {
        //
    }
}
