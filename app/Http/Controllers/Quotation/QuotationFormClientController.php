<?php

namespace App\Http\Controllers\Quotation;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Quotation;
use Illuminate\Http\Request;

class QuotationFormClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_form_from_client(Request $request,Order $order)
    {
        //find quotations
        //$order->update(['status' => 'Orden en gestión']);
        $array = $request->all();
        //$b = [];
        foreach ($array as $clave => $id ){
            $quotation=Quotation::where('id',$clave)->get();
            $quotation[0]->update(['status' => $id]);
            //$quotation=Quotation::where('id',$clave)->get();
            //array_push($b,$quotation) ;
        }
        //return $b;
       

        //range request finding quotations

        //update status $course->update($request->all()); 'status'=> $value
        $user_type = auth()->user()->user_type;
        if ($user_type==1){
            return redirect()->route('order.edit', $order->id)->with('status', 'Gracias por preferirnos, enseguida te llamamos.');
        }elseif ($user_type==2){
            return redirect()->route('order.editRol3', $order->id)->with('status', 'Gracias por preferirnos, enseguida te llamamos.');
        }
        //return redirect()->route('order.edit', $order->id)->with('status', 'Gracias por preferirnos, enseguida te llamamos.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
