<?php

namespace App\Http\Controllers\Quotation;

use App\Http\Controllers\Controller;
use App\Models\Form;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Provider;
use App\Models\Quotation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Inertia\Inertia;

class QuotationPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_quotation(Order $order)
    {
         
        $categories = ProductCategory::with('products')->get();//with('products')->latest()->get();
        //return $categories;
        $products = Product::with('productCategory','form')->get();
        $providers = Provider::all();
        $forms = Form::all();
        $quotations = Quotation::with('form.provider','form.product')->where('order_id',$order->id)->get();
        $mytime = Carbon::now()->toDateTimeString();
        $order->update([
            'quotation_date' => $mytime
        ]);
        
        
        return Inertia::render('Crm/Quotation/Index',[
            'products' => $products,
            'categories' => $categories,
            'order' => $order,
            'providers' => $providers,
            'forms' => $forms,
            'quotations' => $quotations
            
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_quotation(Order $order)
    
    {
       
        
        $categories = ProductCategory::with('products')->get();//with('products')->latest()->get();
        //return $categories;
        $products = Product::with('productCategory','form')->get();
        $providers = Provider::all();
        $forms = Form::all();
        $quotations = Quotation::with('form.provider')->where('order_id',$order->id)->get();
        $mytime = Carbon::now()->toDateTimeString();
        $order->update([
            'quotation_date' => $mytime
        ]);
        
        
        return Inertia::render('Crm/Quotation/Create',[
            'products' => $products,
            'categories' => $categories,
            'order' => $order,
            'providers' => $providers,
            'forms' => $forms,
            'quotations' => $quotations
            
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'order_id' => 'required',
            'form_id' => 'required',
            
            

        ]);
        $quotation = Quotation::create($request->all());
        $order = $quotation->order;
        $mytime = Carbon::now()->toDateTimeString();
        $order->update([
            'quotation_date' => $mytime
        ]);
        return redirect()->route('quotation.edit', $quotation->id)->with('status', 'Cotización creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = ProductCategory::with('products')->get();//with('products')->latest()->get();
        //return $categories;
        $products = Product::with('productCategory','form')->get();
        $providers = Provider::all();
        $forms = Form::all();

        $quotation =  Quotation::with('order','form')->findOrFail($id);
        $quotations = Quotation::with('form.provider')->where('order_id', $quotation->order->id)->get();
        $order = $quotation->order;
        $mytime = Carbon::now()->toDateTimeString();
        $order->update([
            'quotation_date' => $mytime
        ]);
        return Inertia::render('Crm/Quotation/Edit',[
            'products' => $products,
            'categories' => $categories,
            'quotation' => $quotation,
            'providers' => $providers,
            'forms' => $forms,
            'order' => $quotation->order,
            'quotations' => $quotations
            
        ]);

        //$seller = auth()->user()->id;
        
        //$order->update(['seller_id' => $seller]);

       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function messages()
    {
        return [
            'sale_price.required' => 'Please add a name',
        ];
    }

    public function update_description(Request $request, Quotation $quotation)
    {
        $request->validate([
            'sale_price' => 'required',
        ],[
            'required' => 'El campo de precio es requerido'
        ]);
        
        
        $data=$request->all();
        $data;
        $mytime = Carbon::now()->toDateTimeString();
        $order = $quotation->order;
        $order->update([
            'quotation_date' => $mytime
        ]);
        

        $quotation->update([
            'description_1' => $data["description_1"],
            'description_2' => $data["description_2"],
            'description_3' => $data["description_3"],
            'description_4' => $data["description_4"],
            'description_5' => $data["description_5"],
            'sale_price' => $data["sale_price"],
            
        ]);
        return redirect()->back()->with('status', 'descripcion actualizada');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quotation $quotation)
    {
         
        
         $quotation->delete();
         return redirect()->route('quotation.index_quotation', $quotation->order_id)->with('status', 'Cotización eliminada');
    }
}
