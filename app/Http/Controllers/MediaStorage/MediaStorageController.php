<?php

namespace App\Http\Controllers\MediaStorage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MediaStorage;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

class MediaStorageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allMedia = MediaStorage::all();
        return Storage::url($allMedia[0]->name);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $media = $request->media_upload;
        $description = $request->description;

        $mediaStorage = new MediaStorage();
        $mediaStorage->storageMedia($media, $description);

        return $mediaStorage;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(MediaStorage $mediaStorage)
    { 
        return redirect(Storage::url($mediaStorage->name)); 
        /*
        $image = Storage::url($mediaStorage->name);
        return  Inertia::render( 
            [
                'image' => $image

                ]);*/
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
