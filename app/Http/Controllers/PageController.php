<?php

namespace App\Http\Controllers;



use App\Http\Controllers\Controller;
use App\Models\Form;
use App\Models\Lesson;
use App\Models\Oportunity;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Provider;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Storage;


class PageController extends Controller
{
    public function dashboard()
    {
        $userEmail = auth()->user()->email;
        $userId = auth()->user()->id;
        $userType = auth()->user()->user_type;
        $numberOfProducts = 0;
        if($userType==2){

            $Opportunities = Oportunity::with('product','order')->where('status','=', 'Abierta')->get();

            $orders = Order::where('status','=','En espera de cotización')->get();

            $NumberOfOpportunities = count($Opportunities);
            $NumberOfOrders = count($orders);
            
    }elseif($userType==1){
        $Opportunities = Oportunity::where('seller_id',$userId)->where('status','=', 'Abierta')->get();

        $orders = Order::where('status','=','En espera de cotización')->where('seller_id',$userId)->get();

        $NumberOfOpportunities = count($Opportunities);
        $NumberOfOrders = count($orders);
        
        
    }elseif($userType==0){
        $Opportunities = Oportunity::with('product','order')->where('email','=', $userEmail)->get();
        $orders = Order::where('email',$userEmail)->get();
        $products = Order::where('email',$userEmail)->where('status','Orden finalizada')->get();
        $NumberOfOpportunities = count($Opportunities);
        $NumberOfOrders = count($orders);
        $numberOfProducts = count($products);

    }
    return Inertia::render('DashboardHome',[
        'NumberOfOpportunities' => $NumberOfOpportunities,
        'NumberOfOrders' => $NumberOfOrders,
        'NumberOfProducts' => $numberOfProducts,
        
        
        
    ]);
        //return Inertia::render('Dashboard2');
    }

    public function dashboard1()
    {
        $userEmail = auth()->user()->email;
        $userId = auth()->user()->id;
        
        $Opportunities = Oportunity::with('product','order')->where('email','=', $userEmail)->get();
        $orders = Order::where('email',$userEmail)->get();

        return Inertia::render('Dashboard1',[
            'Opportunities' => $Opportunities,
            'Orders' => $orders  
        ]);
        //return Inertia::render('Dashboard2');
    }

    public function dashboard2()
    {
        $categories = ProductCategory::with('products')->get();//with('products')->latest()->get();
        //return $categories;
        $products = Product::with('productCategory','form')->get();
        $orders = Order::latest()->get();
        
        

        return Inertia::render('Dashboard2',[
            'products' => $products,
            'categories' => $categories,
            'orders' => $orders  
            
        ]);
        //return Inertia::render('Dashboard2');
    }

    public function VirtualOffice2()
    {
        $categories = ProductCategory::with('products')->get();
        $products = Product::with('productCategory','form')->get();
        $orders = Order::latest()->get();
        
        

        return Inertia::render('VirtualOffice2',[
            'products' => $products,
            'categories' => $categories,
            'orders' => $orders
                        
        ]);
      
    }

    public function dashboard3()
    {
        $categories = ProductCategory::with('products')->get();//with('products')->latest()->get();
        $products = Product::with('productCategory','form')->get();
        $orders = Order::latest()->get();
        

        $userEmail = auth()->user()->email;
        $userId = auth()->user()->id;
        
        $Opportunities = Oportunity::with('product','order')->get();
        //$Opportunities = Oportunity::with('product','order')->where('email','=', $userEmail)->get();
        
    
        
        
        return Inertia::render('Dashboard3',[
            'products' => $products,
            'categories' => $categories,
            'orders' => $orders,
            'opportunities' => $Opportunities,
            
            
        ]);
      
    }

    public function VirtualOffice3()
    {
        $categories = ProductCategory::with('products')->get();
        $products = Product::with('productCategory','form')->get();
        $orders = Order::latest()->get();
        
        

        return Inertia::render('VirtualOffice3',[
            'products' => $products,
            'categories' => $categories,
            'orders' => $orders
                        
        ]);
      
    }

    public function index()
    {
        $categories = ProductCategory::with('products')->get();//with('products')->latest()->get();
        //return $categories;
        $products = Product::with('productCategory')->get();
        
        
        
        
        return Inertia::render('Home', [
            'products' => $products,
            'canRegister' => true,
            'canLogin' => true,
            'categories' => $categories,
        ]);
    }

  
    public function insurances()
    {
        return Inertia::render('Insurances/Insurances');
    }

    //productos financieros
    public function creditosProductosFinancieros()
    {
        $categories = ProductCategory::with('products')->get();//with('products')->latest()->get();
        //return $categories;
        $products = Product::with('productCategory','form')->get();
        $orders = Order::latest()->get();
        
        

        return Inertia::render('DashboardCredits',[
            'products' => $products,
            'categories' => $categories,
            'orders' => $orders  
            
        ]);
    }

    public function creditosProductosFinancierosRol3()
    {
        $categories = ProductCategory::with('products')->get();//with('products')->latest()->get();
        //return $categories;
        $products = Product::with('productCategory','form')->get();
        $orders = Order::latest()->get();
        
        

        return Inertia::render('DashboardCreditsRol3',[
            'products' => $products,
            'categories' => $categories,
            'orders' => $orders  
            
        ]);
    }

        //manage Products  and Suppliers  temporary route (this route must go in the crud of providers and services )

        public function productsProviders()
        {
            $products = Product::with('productCategory','form')->get();
            $providers = Provider::with('mediaStorage')->get();
            $forms = Form::with('product','provider.mediaStorage')->get();
            return Inertia::render('ProductsProviders',[
                'products' => $products,
                'providers' => $providers,
                'forms' => $forms,


            ]);

        }

        public function productsEdit()
        {
            return Inertia::render('ProductsProviders/Products/ProductsEdit');

        }

        public function productsCreate()
        {
            return Inertia::render('ProductsProviders/Products/ProductsCreate');

        }
        public function ProvidersEdit()
        {
            return Inertia::render('ProductsProviders/Providers/ProvidersEdit');

        }

        public function ProvidersCreate()
        {
            return Inertia::render('ProductsProviders/Providers/ProvidersCreate');

        }


        //manage Products  and Suppliers  temporary route (this route must go in the crud of providers and services )
        public function zurichApitestIndex()
        {
            return Inertia::render('Integrations/Zurich/index');
        }
        public function zurichApitestPrimerMetodo(Request $request)
        {
            $tokem = $request->all();
            
            return Inertia::render('Integrations/Zurich/firstMethod',[
                'tokem' => $tokem
            ]);
        }

        //create users

        public function createProfiles()
        {
            return Inertia::render('CreateProfiles');
        }



    
}
