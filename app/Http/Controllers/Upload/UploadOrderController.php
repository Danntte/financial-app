<?php

namespace App\Http\Controllers\Upload;

use App\Http\Controllers\Controller;
use App\Models\MediaStorage;
use App\Models\Order;
use Illuminate\Http\Request;
use Inertia\Inertia;

class UploadOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mediaStorage = MediaStorage::all();
        return Inertia::render('Crm/Uploads',[
        
        
       
    ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function upload_files(Request $request, Order $order)
    {
        $media = $request->media_upload;
        $description = $request->description;

        $mediaStorage = new MediaStorage();
        $mediaStorage = $mediaStorage->storageMedia($media, $description);
        $order->mediaStorage()->attach($mediaStorage->id);
        return redirect()->back()->with('status', 'archivo cargado');
    }

    public function delete_media(Request $request, Order $order)
    {
        
        $order->mediaStorage()->detach($request->mediaStorage_id);
        return redirect()->back()->with('status', 'archivo eliminado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::with('mediaStorage')->find($id);
        return Inertia::render('Crm/Uploads',[
            'order' => $order,
            'description' => 'Document'
        ]);
    }

    public function validate_purchase($id)
    {
        $order = Order::with('mediaStorage')->find($id);
        return Inertia::render('Crm/UploadsValidarPago',[
            'order' => $order,
            'description' => 'Validation'
        ]);
    }

    public function validate_approved_product($id)
    {
        $order = Order::with('mediaStorage')->find($id);
        return Inertia::render('Crm/UploadsProductoAprovado',[
            'order' => $order,
            'description' => 'ApprovedProduct'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
