<?php

namespace App\Http\Controllers\Rate;

use App\Http\Controllers\Controller;
use App\Models\Rate;
use Illuminate\Http\Request;

class RateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Rate::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response(Rate::create([
            
            'value' => $request->value,
            'user_id' => $request->user_id,
            'course_id' => $request->course_id, 
        ]), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Rate::findOrFail($id)->where("id", $id) -> get(), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rate $rate)
    {
        $rate->fill($request->only([

            'value' ,
            'user_id' ,
            'course_id' , 
         ]));

         // ahora verificamos si cambio algo de lo que se tenia anteriormente
         // isDirty verifica si ha cambiado()  e isClean() hace lo contrario
        if ($rate->isClean()){// si no ha cambiado da un error
        return Response('debe especificar al menos un valor diferente para actualizar',422);
         }
        $rate->save(); // se guardan las modificaciones

          return Response($rate,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rate $rate)
    {
        $rate->delete();
        return response($rate, 200);
    }
}
