<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OnlyAgent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        switch(Auth::user()->user_type){
            case ('2'):
                return $next($request); //if admin to go route
            break;
            case('1'):
                return $next($request); // if agent go to agent route
            break;
            case('0'):
                return redirect('user'); // if user go to user route
            break;
            
        }
        
    }
}
