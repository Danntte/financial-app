<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Opportunities
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        return $next($request);

        switch(Auth::user()->user_type){
            case ('2'):
                return $next($request); //if admin to go HOME
            break;
            case('1'):
                return redirect('opportunities.index'); // if agent go to agent route
            break;
            
            
        }
    }
}
