<?php

use App\Http\Controllers\User\UserController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Agent\AgentController;
use App\Http\Controllers\Course\CourseCategoryController;
use App\Http\Controllers\Course\CourseController;
use App\Http\Controllers\Forum\ForumController;
use App\Http\Controllers\Lesson\LessonController;
use App\Http\Controllers\Product\ProductController;
use App\Http\Controllers\Product\ProductCategoryController;
use App\Http\Controllers\Provider\ProviderController;
use App\Http\Controllers\Order\OrderController;
use App\Http\Controllers\Oportunity\OportunityController;
use App\Http\Controllers\MediaStorage\MediaStorageController;
use App\Http\Controllers\Rate\RateController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
/**
 * User
 */
Route::resource( 'users' , UserController::class );
/**
 * Admin
 */
Route::resource( 'admins' , AdminController::class );
/**
 * Agent
 */
Route::resource( 'agents' , AgentController::class );
/**
 * Courses
 */
//Route::resource( 'courses' , CourseController::class );
/**
 * Course Category
 */
Route::resource( 'courseCategories' , CourseCategoryController::class );
/**
 * Lesson
 */
//Route::resource( 'lessons' , LessonController::class );
/**
 * Rate
 */
Route::resource( 'rates' , RateController::class );
/**
 * Forum
 */
Route::resource( 'forums' , ForumController::class );
/**
 * Products
 */
//Route::resource( 'products' , ProductController::class );
/**
 * Products_Category
 */
Route::resource( 'productCategory' , ProductCategoryController::class );
/**
 * Providers
 */
//Route::resource( 'providers' , ProviderController::class );
/**
 * Orders
 */
Route::resource( 'orders' , OrderController::class );
/**
 * Oportunities
 */
//Route::resource( 'oportunities' , OportunityController::class );



Route::resource( 'mediaStorage' , MediaStorageController::class );

/**
 * zurich integration
 */
//Route::get('zurichApitest/PrimerMetodo', [PageController::class, 'zurichApitestPrimerMetodo'])->name('zurichApitestPrimerMetodo')->middleware(['auth:sanctum','onlyadmin','cors']);