<?php

use App\Http\Controllers\Course\CourseController;
use App\Http\Controllers\Course\CoursePageAdminController;
use App\Http\Controllers\Form\FormMediaStorageController;
use App\Http\Controllers\FormProductProvider\FormProductProviderAdminController;
use App\Http\Controllers\Lesson\LessonAdminController;
use App\Http\Controllers\Library\LibraryPageController;
use App\Http\Controllers\Message\MessageController;
use App\Http\Controllers\Oportunity\FormsController;
use App\Http\Controllers\Oportunity\OportunityPageController;
use App\Http\Controllers\Order\OrderController;
use App\Http\Controllers\Order\OrderPageController;
use App\Models\Course;
use App\Models\MediaStorage;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\PageController;
use App\Http\Controllers\Product\ProductPageController;
use App\Http\Controllers\Quotation\QuotationFormClientController;
use App\Http\Controllers\Quotation\QuotationPageController;
use App\Http\Controllers\Upload\UploadOrderController;
use App\Http\Middleware\OnlyAgent;
use App\Models\Form;
use App\Http\Controllers\Inventory\InventoryController;
use App\Http\Controllers\Portfolio\PortfolioController;
use App\Http\Controllers\Product\ProductAdminController;
use App\Http\Controllers\Provider\ProviderAdminController;
use App\Http\Controllers\User\UserController;
use App\Http\Middleware\OnlyAdmin;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Provider;
use App\Models\MessageRoom;
use App\Models\Message;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PageController::class, 'index'])->name('index');

Route::get('Pqr', function () {
    return "esta es la pagina de pqr"; })
    ->name ("pqr.index")
    ->middleware('auth:sanctum' );

Route::get('dashboard', [PageController::class, 'dashboard'])
    ->middleware('auth:sanctum' )
    ->name('dashboard');

Route::get('dashboard1', [PageController::class, 'dashboard1'])
->middleware('auth:sanctum')
->name('dashboard1');

Route::get('dashboard2', [PageController::class, 'dashboard2'])
->middleware('auth:sanctum' ,'onlyagent')
->name('dashboard2');

Route::get('creditos-productos-financieros', [PageController::class, 'creditosProductosFinancieros'])
->middleware('auth:sanctum' ,'onlyagent')
->name('creditos-productos-financieros');

Route::get('creditos-productos-financieros-admin', [PageController::class, 'creditosProductosFinancierosRol3'])
->middleware('auth:sanctum' ,'onlyagent')
->name('creditos-productos-financieros-rol3');

Route::get('VirtualOffice2', [PageController::class, 'VirtualOffice2'])
->middleware('auth:sanctum' ,'onlyagent')
->name('VirtualOffice2');

Route::get('dashboard3', [PageController::class, 'dashboard3'])
->middleware('auth:sanctum', 'onlyadmin')
->name('dashboard3');

Route::get('VirtualOffice3', [PageController::class, 'VirtualOffice3'])
->middleware('auth:sanctum' ,'onlyagent')
->name('VirtualOffice3');

/**
 * Courses
 */
Route::resource('courses', CoursePageAdminController::class)
    ->middleware(['auth:sanctum','onlyagent']);// route to CRUD courses

/**
 * Lessons admin 
 */
Route::resource('lesson', LessonAdminController::class)
    ->middleware(['auth:sanctum','onlyagent']); //route to }}crud lessons

Route::get('create_lesson/course_/{course}', [LessonAdminController::class, 'create_lesson'])->name('lesson.create_lesson')
    ->middleware(['auth:sanctum','onlyadmin']); //route to }}crud lessons
    
Route::post('lesson_upload_files/{lesson}', [LessonAdminController::class, 'lesson_upload_files'])->name('lesson_upload_files')
->middleware(['auth:sanctum','onlyadmin']); //route to }}crud lessons
Route::post('delete_media/{lesson}', [LessonAdminController::class,'lesson_delete_media'])->name('lesson_delete_media')->middleware(['auth:sanctum','onlyadmin']);



/**
 * Library
 */
Route::resource('libraries', LibraryPageController::class)
    ->middleware(['auth:sanctum', 'onlyagent']);



Route::get('libraries/course/{course:slug}', [LibraryPageController::class, 'course'])->name('libraries.course')
    ->middleware(['auth:sanctum','onlyagent']);

Route::get('lesson/{lesson}', [LibraryPageController::class, 'lesson'])->name('libraries.lesson')
    ->middleware(['auth:sanctum','onlyagent']);
/**
 * CRM Oportunities
 */
Route::resource('opportunities', OportunityPageController::class)->middleware(['auth:sanctum','onlyagent']);
//Route::get('opportunitiesTemporalEdit', [ OportunityPageController::class,'temporalEdit'])->name('opportunities.temporalEdit')->middleware(['auth:sanctum']);
Route::resource('forms', FormsController::class);
Route::get('opportunityFrom/{order}', [ FormsController::class,'storeFromOrder'])->name('storeFromOrder')->middleware(['onlyagent','auth:sanctum']);

/**
 * Oportunities roll 3
 */
Route::get('opportunitiesAdmin', [ OportunityPageController::class,'indexAdmin'])->name('opportunitiesAdmin.index')->middleware(['onlyadmin','auth:sanctum','opportunities']);
Route::post('orderFromOpportunities/{oportunity}',[OportunityPageController::class,'orderFromOpportunities'])->middleware(['onlyagent','auth:sanctum'])->name('orderFromOpportunities');
Route::patch('updateOpportunityAndOrder/{oportunity}',[OportunityPageController::class,'updateOpportunityAndOrder'])->middleware(['onlyagent','auth:sanctum'])->name('updateOpportunityAndOrder');

/**
 * productos 
 */
Route::get('productos/{productos}', [ProductPageController::class, 'show'])->name('productos');
/**
 * segururos 
 */
Route::get('seguros', [PageController::class, 'insurances'])->name('insurances');

/**
 * temporal route for view the  UploadDocs component  in the view cargar
 */
Route::get('cargar', function () {


    
})->middleware(['auth:sanctum','onlyagent']);
/*
 * descargas 
 */

Route::resource('forms.mediaStorage', FormMediaStorageController::class)->middleware(['auth:sanctum','onlyagent']);

Route::get('download/mediaStorage/{id}', [FormMediaStorageController::class, 'downloadFile'])->name('downloadFile')->middleware(['auth:sanctum']);
Route::get('descargas', [FormMediaStorageController::class, 'showFiles'])->middleware(['auth:sanctum','onlyagent'])->name('showFiles');
Route::get('descargas/orden/{order}', [FormMediaStorageController::class, 'downloads_orders'])->name('downloads_orders')->middleware(['auth:sanctum']);

/**
 * Cargas temporal route for view the  UploadDocs component  in the view cargar
 */
Route::resource('cargar', UploadOrderController::class)->middleware(['auth:sanctum']);
route::get('validate/{order}',[UploadOrderController::class,'validate_purchase'])->name('validate_purchase')->middleware(['auth:sanctum']);
route::get('validate_approved_product/{order}',[UploadOrderController::class,'validate_approved_product'])->name('validate_approved_product')->middleware(['auth:sanctum']);
Route::post('cargar/{order}', [UploadOrderController::class,'upload_files'])->name('upload_files')->middleware(['auth:sanctum']);
Route::post('delete_media/{order}', [UploadOrderController::class,'delete_media'])->name('delete_media')->middleware(['auth:sanctum']);

/**
 * Cotizaciones 
 */

Route::get('cotizacion/orden/{id}', [OrderController::class,'orderQuotation'])->name('quotations')->middleware(['auth:sanctum']);
   //->middleware(['auth:sanctum'])

Route::resource('quotation', QuotationPageController::class);
Route::get('quotation/order/{order}',[QuotationPageController::class, 'index_quotation'])->name('quotation.index_quotation')->middleware(['auth:sanctum','onlyagent']);

Route::get('create_quotation/order/{order}', [QuotationPageController::class, 'create_quotation'])->name('quotation.create_quotation')
    ->middleware(['auth:sanctum','onlyagent']); //route to }}crud lessons
Route::patch('quotation/quotation{quotation}/descriptionUpdated', [QuotationPageController::class, 'update_description'])->name('update_quotation_description')->middleware(['auth:sanctum']);

/*
*Rutas para Mensajes
*/
//Route::get('roomsVue', [MessageController::class, 'roomsVue'])->middleware('auth:sanctum')->name('room/roomsVue');
Route::get('roomIndex/{roomId}', [MessageController::class, 'roomIndex'])->middleware('auth:sanctum')->name('roomIndex');
Route::get('rooms', [MessageController::class, 'rooms'])->name('rooms')->middleware('auth:sanctum')->name('rooms');
Route::post('rooms/{roomId}/newMessage', [MessageController::class, 'newMessage'])->middleware('auth:sanctum')->name('newMessage');
Route::post('rooms/{roomId}/messages', [MessageController::class, 'messages'])->middleware('auth:sanctum')->name('getMessages');

Route::put('cotizacion/orden/{order}/form_user',[QuotationFormClientController::class,'store_form_from_client'])->name('quotation_form_user.store')->middleware(['auth:sanctum','pdfgeneratebutton']);

/**
 * orders
 */
Route::resource('order', OrderPageController::class)->middleware(['auth:sanctum']);
Route::put('order/order{order}/titleUpdated', [OrderPageController::class, 'update_titles'])->name('update_titles')->middleware(['auth:sanctum']);


Route::put('order/order{order}/headerUpdated', [OrderPageController::class, 'update_headers'])->name('update_headers')->middleware(['auth:sanctum']);
Route::put('order/order{order}/submitedOrder', [OrderPageController::class, 'submitedOrder'])->name('order.submitedOrder')->middleware(['auth:sanctum']);
Route::get('order/order{order}/goToOrder', [OrderPageController::class, 'goToOrder'])->name('order.goToOrder')->middleware(['auth:sanctum', 'pdfgeneratebutton']);
Route::put('order/order{order}/statusToStandby', [OrderPageController::class, 'statusToStandby'])->name('order.statusToStandby')->middleware(['auth:sanctum']);
Route::put('order/order{order}/statusToQuotationSubmited', [OrderPageController::class, 'statusToQuotationSubmited'])->name('order.statusToQuotationSubmited')->middleware(['auth:sanctum']);
Route::put('order/order{order}/statusToApproved', [OrderPageController::class, 'statusToApproved'])->name('order.statusToApproved')->middleware(['auth:sanctum','onlyagent']);//aqui el midleware debe ser onlyadmin
Route::put('order/order{order}/statusToCancelled', [OrderPageController::class, 'statusToCancelled'])->name('order.statusToCancelled')->middleware(['auth:sanctum','onlyagent']);
Route::put('order/order{order}/statusToFinished', [OrderPageController::class, 'statusToFinished'])->name('order.statusToFinished')->middleware(['auth:sanctum','onlyagent']);
Route::get('order/order{order}/goToLink_payment', [OrderPageController::class, 'goToLink_payment'])->name('order.goToLink_payment')->middleware(['auth:sanctum','cors']);
Route::patch('order/order{order}/updatePaymentLink', [OrderPageController::class, 'updatePaymentLink'])->name('order.updatePaymentLink')->middleware(['auth:sanctum','onlyadmin']);
Route::get('order/{order}/adminEdit', [OrderPageController::class, 'editRol3'])->name('order.editRol3')->middleware(['auth:sanctum']);


/**
 * Inventory 
 */
Route::resource('inventory', InventoryController::class)->middleware(['auth:sanctum']);


//Route::get('inventory/edit', [InventoryController::class, 'inventoryEdit'])->name('inventoryEdit');
Route::get('inventoryAdmin', [InventoryController::class, 'inventoryAdmin'])->middleware(['auth:sanctum'])->name('inventoryAdmin');
Route::get('inventoryMain', [InventoryController::class, 'inventoryMain'])->middleware(['auth:sanctum'])->name('inventoryMain');
Route::post('inventory/createOpportunity', [InventoryController::class, 'createOpportunity'])->middleware(['auth:sanctum'])->name('inventory.createOpportunity');
Route::post('inventory/upload_files{inventory}', [InventoryController::class, 'inventory_upload_files'])->middleware(['auth:sanctum'])->name('inventory.upload_files');

Route::post('inventory/delete_media{inventory}', [InventoryController::class, 'inventory_delete_media'])->middleware(['auth:sanctum'])->name('inventory.delete_media');

/**
 *  inventory routes
 */

 /**
 * CRUD products, providers and forms
 */

Route::resource('products', ProductAdminController::class)->middleware(['auth:sanctum','onlyadmin']);
Route::resource('providers', ProviderAdminController::class)->middleware(['auth:sanctum','onlyadmin']);
Route::get('productsProviders', [PageController::class, 'productsProviders'])->name('productsProviders')->middleware(['auth:sanctum','onlyadmin']);
Route::resource('formsProductsProviders', FormProductProviderAdminController::class)->middleware(['auth:sanctum','onlyadmin']);
Route::post('form_upload_files/{form}', [FormProductProviderAdminController::class, 'form_upload_files'])->name('form_upload_files')
->middleware(['auth:sanctum','onlyadmin']);
Route::post('form_delete_media/{form}', [FormProductProviderAdminController::class,'form_delete_media'])->name('form_delete_media')->middleware(['auth:sanctum','onlyadmin']);

/**
 * CRUD products and suppliers
 */

 /**
  * Zurich api
  */
  Route::get('zurichApitest', [PageController::class, 'zurichApitestIndex'])->name('zurichApitestIndex')->middleware(['auth:sanctum','onlyadmin']);
  Route::get('zurichApitest/PrimerMetodo', [PageController::class, 'zurichApitestPrimerMetodo'])->name('zurichApitestPrimerMetodo')->middleware(['auth:sanctum','onlyadmin','cors']);

  /**
   * create profiles
   */

  Route::get('createProfiles', [PageController::class, 'createProfiles'])->name('createProfiles')->middleware(['auth:sanctum','onlyadmin']);
  Route::resource('users', UserController::class)->middleware(['auth:sanctum','onlyadmin']);