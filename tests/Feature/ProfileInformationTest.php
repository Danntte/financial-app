<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProfileInformationTest extends TestCase
{
    use RefreshDatabase;

    public function test_profile_information_can_be_updated()
    {
        $this->actingAs($user = User::factory()->create());

        $response = $this->put('/user/profile-information', [
            'firstname' => 'Test firstName',
            'lastname' => 'Test lastName',
            'email' => 'test@example.com',
            'birthday' => '1999-06-11 00:00:00',
            
            
        ]);
        $this->assertEquals('Test firstName', $user->fresh()->firstname);
        $this->assertEquals('test@example.com', $user->fresh()->email);
        
        
    }
}
